var SWUERC20Implementation = artifacts.require("./flatten/SWUERC20Implementation.sol");
var ERC20UpgradeableProxy = artifacts.require("./flatten/ERC20UpgradeableProxy.sol");

var CrowdSaleProxy = artifacts.require("./flatten/CrowdSaleProxy.sol");
var CrowdSaleImplementation = artifacts.require("./flatten/CrowdSaleImplementation.sol");

var WhiteList = artifacts.require("./flatten/WhiteList.sol");



//to deploy on bscTestnet run: truffle migrate --reset --network bscTestnet

module.exports = async function (deployer, network, accounts) {

    console.log("Token contracts deploy...");

    let proxyInst, erc20TokenImplementationInst;

    let proxyAdmin = accounts[0];
    let tokenAdmin = accounts[1];
    let platformFeesWallet = "0xB4B236A7B1CFa644Dcd3420C7c7B8C6f708DD555";
    let compteFondCollecte = "0x4b7b08F11d201e9dcE30B5Be7AC56D312A398E73";
     

    let tokenProps = {
        name: 'SWU',
        symbol: 'SWU',
        totalSupply: BigInt("100000000000000000000000000"),
        decimals: 18,
        mintable: true,
        admin: tokenAdmin,
        transferBlocked: true,
    }

    let crowdSaleProps = {
        openingTime : (new Date("07/27/2021 22:30").getTime()/ 1000), //MM/JJ/YYYY (Heures-2)
        closingTime : (new Date("09/15/2121 18:55").getTime()/ 1000),
        rate : 1250000,
        platformFeesRate : 0000000,
        wallet : compteFondCollecte,
        //paymentToken : "0xed24fc36d5ee211ea25a80239fb8c4cfd80f12ee",
        paymentToken : "0x860aA68b0644b1533bb5E0a988Cf57B0eBdEb780", //DAI
        owner : tokenAdmin,
        platformFeesWallet : platformFeesWallet
    }

    await deployer.deploy(SWUERC20Implementation, { from: proxyAdmin })

    erc20TokenImplementationInst = await SWUERC20Implementation.deployed({ from: proxyAdmin });
    console.log('Token address: ', erc20TokenImplementationInst.address);

    await deployer.deploy(
        ERC20UpgradeableProxy,
        erc20TokenImplementationInst.address,
        proxyAdmin,
        "0x");

    proxyInst = await ERC20UpgradeableProxy.deployed();
    console.log('Proxy address: ', proxyInst.address);


    console.log("Init proxy tokens");

    let myUpgredableToken = await SWUERC20Implementation.at(proxyInst.address);

    await myUpgredableToken.initialize(tokenProps.name,
        tokenProps.symbol,
        tokenProps.decimals,
        tokenProps.totalSupply,
        tokenProps.mintable,
        tokenProps.admin,
        tokenProps.transferBlocked,
        { from: tokenAdmin }).then();
    console.log("contract initialized");

    let decimals = await myUpgredableToken.decimals.call({ from: tokenAdmin });
    let name = await myUpgredableToken.name.call({ from: tokenAdmin });
    let totalSupply = await myUpgredableToken.totalSupply.call({ from: tokenAdmin });
    let tokenOwner = await myUpgredableToken.getOwner.call({ from: tokenAdmin });
    console.log("name: ", name);
    console.log("totalSupply: ", totalSupply);
    console.log("decimals: ", decimals);
    console.log("owner: ", tokenOwner);


    console.log("deploy whitelist");
    
    //let whitelistContract = await WhiteList.at("0x0AA25C1ED107e9fbf121d9A7c835907ACbC45a0E");

    await deployer.deploy(WhiteList, { from: proxyAdmin });

    let whitelistContract = await WhiteList.deployed();

    await whitelistContract.addWhitelistAdmin(tokenAdmin, { from : proxyAdmin}).then();

    console.log("Whitelist contract deployed");


    console.log("Deploy crowdsale contracts")


    await deployer.deploy(
        CrowdSaleImplementation,
        crowdSaleProps.openingTime,
        crowdSaleProps.closingTime,
        crowdSaleProps.rate,
        crowdSaleProps.platformFeesRate,
        crowdSaleProps.wallet,
        crowdSaleProps.platformFeesWallet,
        proxyInst.address,
        whitelistContract.address,
        crowdSaleProps.paymentToken,
        { from: proxyAdmin })

    upgredableERC20PaymentCrowdsaleInst = await CrowdSaleImplementation.deployed({ from: proxyAdmin });
    console.log('Token address: ', upgredableERC20PaymentCrowdsaleInst.address);

    await deployer.deploy(
        CrowdSaleProxy,
        upgredableERC20PaymentCrowdsaleInst.address,
        proxyAdmin,
        "0x");

    crowdSaleProxyInst = await CrowdSaleProxy.deployed();
    console.log('Crowdsale Proxy address: ', crowdSaleProxyInst.address);


    console.log("Init proxy crowdsale");

    let myUpgredableCrowdsale = await CrowdSaleImplementation.at(crowdSaleProxyInst.address);

    await myUpgredableCrowdsale.initialize(
        crowdSaleProps.openingTime,
        crowdSaleProps.closingTime,
        crowdSaleProps.rate,
        crowdSaleProps.platformFeesRate,
        crowdSaleProps.wallet,
        crowdSaleProps.platformFeesWallet,
        proxyInst.address,
        whitelistContract.address,
        crowdSaleProps.paymentToken,
        crowdSaleProps.owner,
        { from: tokenAdmin }).then();
    console.log("contract initialized");


    console.log("set crowdsale contract on bep20 contract");

    await myUpgredableToken.changeCrowdSaleContractAddress(myUpgredableCrowdsale.address,{from:tokenAdmin}).then();
    let crowdSaleContractAddress = await myUpgredableToken._crowdSaleContractAddress.call({from:tokenAdmin});
    
    console.log("Crowdsale contract address on erc20 contract: "+crowdSaleContractAddress);
    console.log("Migration done");

    console.log("Token contract: ",SWUERC20Implementation.address);
    console.log("Token contract proxy: ",ERC20UpgradeableProxy.address);
    console.log("Whitelist contract: ",WhiteList.address);
    console.log("crowdsale contract: ",CrowdSaleImplementation.address);
    console.log("crowdsale proxy: ",CrowdSaleProxy.address);
    console.log("payement contract: ",crowdSaleProps.paymentToken);
    console.log("owner: ",tokenOwner);


};