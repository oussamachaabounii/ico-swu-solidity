// SPDX-License-Identifier: MIT
pragma solidity >=0.6.0;

import "../ITransferVerificationModule.sol";
import "../../../access/roles/WhiteListedRole.sol";

contract WhiteList is ITransferVerificationModule, WhiteListedRole{
    function isAutorized(address _address) external override(ITransferVerificationModule) view returns (bool){
        return _isWhitelisted(_address);
    }
}