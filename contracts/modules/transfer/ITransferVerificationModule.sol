// SPDX-License-Identifier: MIT
pragma solidity >=0.6.0;
/**
* @notice module to check if user can do transfert or not
 */
interface ITransferVerificationModule{
    function isAutorized(address _address) external view returns (bool);
}