package crowdsale.contract;

import io.reactivex.Flowable;
import io.reactivex.functions.Function;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import org.web3j.abi.EventEncoder;
import org.web3j.abi.FunctionEncoder;
import org.web3j.abi.TypeReference;
import org.web3j.abi.datatypes.Address;
import org.web3j.abi.datatypes.Bool;
import org.web3j.abi.datatypes.Event;
import org.web3j.abi.datatypes.Type;
import org.web3j.abi.datatypes.generated.Uint256;
import org.web3j.crypto.Credentials;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameter;
import org.web3j.protocol.core.RemoteCall;
import org.web3j.protocol.core.RemoteFunctionCall;
import org.web3j.protocol.core.methods.request.EthFilter;
import org.web3j.protocol.core.methods.response.BaseEventResponse;
import org.web3j.protocol.core.methods.response.Log;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.tuples.generated.Tuple2;
import org.web3j.tx.Contract;
import org.web3j.tx.TransactionManager;
import org.web3j.tx.gas.ContractGasProvider;

/**
 * <p>Auto generated code.
 * <p><strong>Do not modify!</strong>
 * <p>Please use the <a href="https://docs.web3j.io/command_line.html">web3j command line tools</a>,
 * or the org.web3j.codegen.SolidityFunctionWrapperGenerator in the 
 * <a href="https://github.com/web3j/web3j/tree/master/codegen">codegen module</a> to update.
 *
 * <p>Generated with web3j version 1.4.1.
 */
@SuppressWarnings("rawtypes")
public class CrowdSaleImplementationV1 extends Contract {
    public static final String BINARY = "60806040523480156200001157600080fd5b5060405162001eeb38038062001eeb83398181016040526101208110156200003857600080fd5b508051602082015160408301516060840151608085015160a086015160c087015160e08801516101009098015196979596949593949293919290919088888888888888888888888888888888888886620000d9576040805162461bcd60e51b815260206004820152601a60248201527f43726f776473616c653a20496e76616c696420726174652e2e2e000000000000604482015290519081900360640190fd5b6001600160a01b038516620001205760405162461bcd60e51b815260040180806020018281038252602a81526020018062001e5a602a913960400191505060405180910390fd5b6001600160a01b038416620001675760405162461bcd60e51b815260040180806020018281038252603881526020018062001eb36038913960400191505060405180910390fd5b6001600160a01b038316620001ae5760405162461bcd60e51b815260040180806020018281038252602f81526020018062001e84602f913960400191505060405180910390fd5b42891015620001ef5760405162461bcd60e51b815260040180806020018281038252602681526020018062001e0e6026913960400191505060405180910390fd5b88881015620002305760405162461bcd60e51b815260040180806020018281038252602681526020018062001e346026913960400191505060405180910390fd5b600a8054336001600160a01b03199182161790915560008a90556001899055600688905560078790556004805482166001600160a01b038881169190911790915560058054831687831617905560038054909216908516179055620002958262000339565b620002a0816200038d565b604080518a8152602081018a9052808201899052606081018890526001600160a01b03808816608083015280871660a083015280861660c083015280851660e0830152831661010082015290517f308450436833f65b26161c96864c2ce805f66bddc6b71886c7729df8eff3e437918190036101200190a1505050505050505050505050505050505050505050505050505050620003e1565b600b80546001600160a01b0383166001600160a01b0319909116811790915560408051918252517f86eba8651458cc924e4911e8a0a31258558de0474fdc43da05cea932cf130aad9181900360200190a150565b600280546001600160a01b0383166001600160a01b0319909116811790915560408051918252517fbd4032c1c91da2791730ea1bbc82c6b6f857da7c0a8318143d19ef74e62cd9139181900360200190a150565b611a1d80620003f16000396000f3fe608060405234801561001057600080fd5b50600436106101cf5760003560e01c80638696038b11610104578063bb4ce285116100a2578063ef2485b511610071578063ef2485b514610428578063f16881c11461045e578063f2fde38b1461047b578063fc0c546a146104a1576101cf565b8063bb4ce285146103c0578063c2507ac1146103dd578063d417bf03146103fa578063d81237d214610420576101cf565b806393e59dc1116100de57806393e59dc11461038b578063a3974ec114610393578063b36ffca6146103b0578063b7a8807c146103b8576101cf565b80638696038b146102fd5780638c0d9dca1461031a5780638da5cb5b14610383576101cf565b80634042b66f1161017157806369ea17711161014b57806369ea1771146102aa5780636a326ab1146102c75780636a448093146102ed578063715018a6146102f5576101cf565b80634042b66f146102925780634b6753bc1461029a578063521eb273146102a2576101cf565b80632c4e722e116101ad5780632c4e722e146102445780633013ce291461025e57806331c3aef81461028257806335382fdc1461028a576101cf565b80630752881a146101d457806312f26140146102025780631515bc2b14610228575b600080fd5b610200600480360360408110156101ea57600080fd5b506001600160a01b0381351690602001356104a9565b005b6102006004803603602081101561021857600080fd5b50356001600160a01b03166105fc565b610230610660565b604080519115158252519081900360200190f35b61024c610668565b60408051918252519081900360200190f35b61026661066e565b604080516001600160a01b039092168252519081900360200190f35b61026661067d565b61024c61068c565b61024c610692565b61024c610698565b61026661069e565b610200600480360360208110156102c057600080fd5b50356106ad565b610200600480360360208110156102dd57600080fd5b50356001600160a01b0316610740565b6102666107a1565b6102006107b0565b6102006004803603602081101561031357600080fd5b5035610852565b610200600480360361014081101561033157600080fd5b508035906020810135906040810135906060810135906001600160a01b03608082013581169160a081013582169160c082013581169160e08101358216916101008201358116916101200135166108e5565b610266610c32565b610266610c41565b61024c600480360360208110156103a957600080fd5b5035610c50565b61024c610c61565b61024c610c67565b61024c600480360360208110156103d657600080fd5b5035610c6d565b61024c600480360360208110156103f357600080fd5b5035610c78565b6102006004803603602081101561041057600080fd5b50356001600160a01b0316610c83565b61024c610d35565b6104456004803603602081101561043e57600080fd5b5035610d3b565b6040805192835260208301919091528051918290030190f35b6102006004803603602081101561047457600080fd5b5035610d6c565b6102006004803603602081101561049157600080fd5b50356001600160a01b0316610dff565b610266610ef8565b806104b48382610f07565b600254604080516323b872dd60e01b81523360048201523060248201526044810185905290516001600160a01b03909216916323b872dd916064808201926020929091908290030181600087803b15801561050e57600080fd5b505af1158015610522573d6000803e3d6000fd5b505050506040513d602081101561053857600080fd5b50600090506105468261124e565b9050600061055383611287565b60095490915061056390826112a6565b60095560006105728483611307565b60085490915061058290826112a6565b60085561058f8684611349565b604080518581526020810185905281516001600160a01b0389169233927f623b3804fa71d67900d064613da8f94b9617215ee90799290593e1745087ad18929081900390910190a36105e18685611353565b6105ea85611357565b6105f48685611353565b505050505050565b610604611482565b600a546001600160a01b03908116911614610654576040805162461bcd60e51b8152602060048201819052602482015260008051602061196f833981519152604482015290519081900360640190fd5b61065d81611486565b50565b600154421190565b60065481565b6002546001600160a01b031681565b6005546001600160a01b031681565b60075481565b60085481565b60015481565b6004546001600160a01b031681565b6106b5611482565b600a546001600160a01b03908116911614610705576040805162461bcd60e51b8152602060048201819052602482015260008051602061196f833981519152604482015290519081900360640190fd5b60068190556040805182815290517fe65c987b2e4668e09ba867026921588005b2b2063607a1e7e7d91683c8f91b7b9181900360200190a150565b610748611482565b600a546001600160a01b03908116911614610798576040805162461bcd60e51b8152602060048201819052602482015260008051602061196f833981519152604482015290519081900360640190fd5b61065d816114da565b600d546001600160a01b031681565b6107b8611482565b600a546001600160a01b03908116911614610808576040805162461bcd60e51b8152602060048201819052602482015260008051602061196f833981519152604482015290519081900360640190fd5b600a546040516000916001600160a01b0316907f8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0908390a3600a80546001600160a01b0319169055565b61085a611482565b600a546001600160a01b039081169116146108aa576040805162461bcd60e51b8152602060048201819052602482015260008051602061196f833981519152604482015290519081900360640190fd5b600c8190556040805182815290517fc7aa767594ec9483c27a7b55f1752d99a767a64eb3b637e8f768cb9a20f994b39181900360200190a150565b600b54600160a81b900460ff1680610900575061090061152e565b806109155750600b54600160a01b900460ff16155b6109505760405162461bcd60e51b815260040180806020018281038252602e8152602001806118fc602e913960400191505060405180910390fd5b600b54600160a81b900460ff1615801561098757600b805460ff60a01b1960ff60a81b19909116600160a81b1716600160a01b1790555b600089116109dc576040805162461bcd60e51b815260206004820152601a60248201527f43726f776473616c653a20496e76616c696420726174652e2e2e000000000000604482015290519081900360640190fd5b6001600160a01b038716610a215760405162461bcd60e51b815260040180806020018281038252602a81526020018061198f602a913960400191505060405180910390fd5b6001600160a01b038616610a665760405162461bcd60e51b81526004018080602001828103825260388152602001806118366038913960400191505060405180910390fd5b6001600160a01b038516610aab5760405162461bcd60e51b815260040180806020018281038252602f8152602001806119b9602f913960400191505060405180910390fd5b428b1015610aea5760405162461bcd60e51b81526004018080602001828103825260268152602001806118106026913960400191505060405180910390fd5b8a8a1015610b295760405162461bcd60e51b81526004018080602001828103825260268152602001806118946026913960400191505060405180910390fd5b60008b905560018a905560068990556007889055600480546001600160a01b03808a166001600160a01b0319928316179092556005805489841690831617905560038054888416908316179055600a805492851692909116919091179055610b9084611486565b610b99836114da565b604080518c8152602081018c90528082018b9052606081018a90526001600160a01b03808a16608083015280891660a083015280881660c083015280871660e0830152851661010082015290517f308450436833f65b26161c96864c2ce805f66bddc6b71886c7729df8eff3e437918190036101200190a18015610c2557600b805460ff60a81b191690555b5050505050505050505050565b600a546001600160a01b031690565b600b546001600160a01b031681565b6000610c5b82611287565b92915050565b60095481565b60005481565b6000610c5b82611534565b6000610c5b8261124e565b610c8b611482565b600a546001600160a01b03908116911614610cdb576040805162461bcd60e51b8152602060048201819052602482015260008051602061196f833981519152604482015290519081900360640190fd5b600d80546001600160a01b0319166001600160a01b03838116919091179182905560408051929091168252517f259d3cb0468884333d5645aa57882df877c1e49ba83d2cb06926a1805da61e29916020908290030190a150565b600c5481565b6000806000610d498461124e565b90506000610d5682611534565b9050610d628282611307565b9350915050915091565b610d74611482565b600a546001600160a01b03908116911614610dc4576040805162461bcd60e51b8152602060048201819052602482015260008051602061196f833981519152604482015290519081900360640190fd5b60078190556040805182815290517f7868ce6a2369eec6eb534f79f28e0ded09a143bd858c89743aac2b84996b906c9181900360200190a150565b610e07611482565b600a546001600160a01b03908116911614610e57576040805162461bcd60e51b8152602060048201819052602482015260008051602061196f833981519152604482015290519081900360640190fd5b6001600160a01b038116610e9c5760405162461bcd60e51b815260040180806020018281038252602681526020018061186e6026913960400191505060405180910390fd5b600a546040516001600160a01b038084169216907f8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e090600090a3600a80546001600160a01b0319166001600160a01b0392909216919091179055565b6003546001600160a01b031681565b6000544210158015610f1b57506001544211155b610f565760405162461bcd60e51b815260040180806020018281038252602481526020018061192a6024913960400191505060405180910390fd5b600b5460408051634f0d793560e11b81526001600160a01b03858116600483015291519190921691639e1af26a916024808301926020929190829003018186803b158015610fa357600080fd5b505afa158015610fb7573d6000803e3d6000fd5b505050506040513d6020811015610fcd57600080fd5b5051611020576040805162461bcd60e51b815260206004820152601f60248201527f43726f776473616c653a2075736572206e6f742077686974656c697374656400604482015290519081900360640190fd5b6001600160a01b0382166110655760405162461bcd60e51b81526004018080602001828103825260218152602001806118ba6021913960400191505060405180910390fd5b806110b7576040805162461bcd60e51b815260206004820152601f60248201527f43726f776473616c653a20496e76616c696420776569416d6f756e742e2e2e00604482015290519081900360640190fd5b60025460408051636eb1769f60e11b8152336004820152306024820152905183926001600160a01b03169163dd62ed3e916044808301926020929190829003018186803b15801561110757600080fd5b505afa15801561111b573d6000803e3d6000fd5b505050506040513d602081101561113157600080fd5b50511015611186576040805162461bcd60e51b815260206004820152601f60248201527f5061796d656e7420746f6b656e20616c6c6f77616e636520746f6f206c6f7700604482015290519081900360640190fd5b60006111918261124e565b600354604080516370a0823160e01b815230600482015290519293506001600160a01b03909116916370a0823191602480820192602092909190829003018186803b1580156111df57600080fd5b505afa1580156111f3573d6000803e3d6000fd5b505050506040513d602081101561120957600080fd5b50518111156112495760405162461bcd60e51b81526004018080602001828103825260218152602001806118db6021913960400191505060405180910390fd5b505050565b600061126361125c83611287565b8390611307565b9150610c5b620f42406112816006548561155390919063ffffffff16565b906115ac565b6000610c5b6305f5e1006112816007548561155390919063ffffffff16565b600082820183811015611300576040805162461bcd60e51b815260206004820152601b60248201527f536166654d6174683a206164646974696f6e206f766572666c6f770000000000604482015290519081900360640190fd5b9392505050565b600061130083836040518060400160405280601e81526020017f536166654d6174683a207375627472616374696f6e206f766572666c6f7700008152506115ee565b6113538282611685565b5050565b600061136282611287565b6002546005546040805163a9059cbb60e01b81526001600160a01b03928316600482015260248101859052905193945091169163a9059cbb916044808201926020929091908290030181600087803b1580156113bd57600080fd5b505af11580156113d1573d6000803e3d6000fd5b505050506040513d60208110156113e757600080fd5b50506002546004546001600160a01b039182169163a9059cbb911661140c8585611307565b6040518363ffffffff1660e01b815260040180836001600160a01b0316815260200182815260200192505050602060405180830381600087803b15801561145257600080fd5b505af1158015611466573d6000803e3d6000fd5b505050506040513d602081101561147c57600080fd5b50505050565b3390565b600b80546001600160a01b0383166001600160a01b0319909116811790915560408051918252517f86eba8651458cc924e4911e8a0a31258558de0474fdc43da05cea932cf130aad9181900360200190a150565b600280546001600160a01b0383166001600160a01b0319909116811790915560408051918252517fbd4032c1c91da2791730ea1bbc82c6b6f857da7c0a8318143d19ef74e62cd9139181900360200190a150565b303b1590565b6000610c5b6305f5e100611281600c548561155390919063ffffffff16565b60008261156257506000610c5b565b8282028284828161156f57fe5b04146113005760405162461bcd60e51b815260040180806020018281038252602181526020018061194e6021913960400191505060405180910390fd5b600061130083836040518060400160405280601a81526020017f536166654d6174683a206469766973696f6e206279207a65726f0000000000008152506117aa565b6000818484111561167d5760405162461bcd60e51b81526004018080602001828103825283818151815260200191508051906020019080838360005b8381101561164257818101518382015260200161162a565b50505050905090810190601f16801561166f5780820380516001836020036101000a031916815260200191505b509250505060405180910390fd5b505050900390565b600061169082611534565b600354600d546040805163a9059cbb60e01b81526001600160a01b03928316600482015260248101859052905193945091169163a9059cbb916044808201926020929091908290030181600087803b1580156116eb57600080fd5b505af11580156116ff573d6000803e3d6000fd5b505050506040513d602081101561171557600080fd5b50506003546001600160a01b031663a9059cbb846117338585611307565b6040518363ffffffff1660e01b815260040180836001600160a01b0316815260200182815260200192505050602060405180830381600087803b15801561177957600080fd5b505af115801561178d573d6000803e3d6000fd5b505050506040513d60208110156117a357600080fd5b5050505050565b600081836117f95760405162461bcd60e51b815260206004820181815283516024840152835190928392604490910191908501908083836000831561164257818101518382015260200161162a565b50600083858161180557fe5b049594505050505056fe54696d656443726f776473616c653a20496e76616c6964206f70656e696e6754696d652e2e2e43726f776473616c653a20496e76616c696420706c617466726f6d2077616c6c657420666565732c207a65726f20616464726573732e2e2e4f776e61626c653a206e6577206f776e657220697320746865207a65726f206164647265737354696d656443726f776473616c653a20496e76616c696420636c6f73696e6754696d652e2e2e43726f776473616c653a20496e76616c69642062656e65666963696172792e2e2e546f6b656e7320746f207265636569766520657863656564732062616c616e6365496e697469616c697a61626c653a20636f6e747261637420697320616c726561647920696e697469616c697a656454696d656443726f776473616c653a2049434f206973206e6f74206f70656e65642e2e2e536166654d6174683a206d756c7469706c69636174696f6e206f766572666c6f774f776e61626c653a2063616c6c6572206973206e6f7420746865206f776e657243726f776473616c653a20496e76616c69642077616c6c65742c207a65726f20616464726573732e2e2e43726f776473616c653a20496e76616c696420455243323020546f6b656e2c207a65726f20616464726573732e2e2ea2646970667358221220269cb46e0e2984ced5b7c11210b04c77485cbb5f2edac18684374c2b7d2eefff64736f6c634300060c003354696d656443726f776473616c653a20496e76616c6964206f70656e696e6754696d652e2e2e54696d656443726f776473616c653a20496e76616c696420636c6f73696e6754696d652e2e2e43726f776473616c653a20496e76616c69642077616c6c65742c207a65726f20616464726573732e2e2e43726f776473616c653a20496e76616c696420455243323020546f6b656e2c207a65726f20616464726573732e2e2e43726f776473616c653a20496e76616c696420706c6174666f726d2077616c6c657420666565732c207a65726f20616464726573732e2e2e";

    public static final String FUNC_BUYTOKENS = "buyTokens";

    public static final String FUNC_CLOSINGTIME = "closingTime";

    public static final String FUNC_FASTBONUSTRANSFERRATE = "fastBonusTransferRate";

    public static final String FUNC_FASTBONUSWALLET = "fastBonusWallet";

    public static final String FUNC_GETFASTBONUSAMOUNTTOSUBTRACT = "getFastBonusAmountToSubtract";

    public static final String FUNC_GETPLATFORMFEESAMOUNT = "getPlatformFeesAmount";

    public static final String FUNC_GETTOKENAMOUNT = "getTokenAmount";

    public static final String FUNC_GETTOKENTORECEIVE = "getTokenToReceive";

    public static final String FUNC_HASCLOSED = "hasClosed";

    public static final String FUNC_INITIALIZE = "initialize";

    public static final String FUNC_OPENINGTIME = "openingTime";

    public static final String FUNC_OWNER = "owner";

    public static final String FUNC_PAYMENTTOKEN = "paymentToken";

    public static final String FUNC_PLATFORMFEESCOLLECTED = "platformFeesCollected";

    public static final String FUNC_PLATFORMFEESRATE = "platformFeesRate";

    public static final String FUNC_PLATFORMFEESWALLET = "platformFeesWallet";

    public static final String FUNC_RATE = "rate";

    public static final String FUNC_RENOUNCEOWNERSHIP = "renounceOwnership";

    public static final String FUNC_SETPAYMENTTOKEN = "setPaymentToken";

    public static final String FUNC_SETWHITELISTCONTRACT = "setWhitelistContract";

    public static final String FUNC_TOKEN = "token";

    public static final String FUNC_TRANSFEROWNERSHIP = "transferOwnership";

    public static final String FUNC_UPDATEFASTBONUSTRANSFERRATE = "updateFastBonusTransferRate";

    public static final String FUNC_UPDATEFASTBONUSWALLET = "updateFastBonusWallet";

    public static final String FUNC_UPDATEPLATFORMFEESRATE = "updatePlatformFeesRate";

    public static final String FUNC_UPDATERATE = "updateRate";

    public static final String FUNC_WALLET = "wallet";

    public static final String FUNC_WEIRAISED = "weiRaised";

    public static final String FUNC_WHITELIST = "whitelist";

    public static final Event CONTRACTDEPLOYED_EVENT = new Event("ContractDeployed", 
            Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}, new TypeReference<Uint256>() {}, new TypeReference<Uint256>() {}, new TypeReference<Uint256>() {}, new TypeReference<Address>() {}, new TypeReference<Address>() {}, new TypeReference<Address>() {}, new TypeReference<Address>() {}, new TypeReference<Address>() {}));
    ;

    public static final Event FASTBONUSTRANSFERRATEUPDATED_EVENT = new Event("FastBonusTransferRateUpdated", 
            Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}));
    ;

    public static final Event FASTBONUSWALLETUPDATED_EVENT = new Event("FastBonusWalletUpdated", 
            Arrays.<TypeReference<?>>asList(new TypeReference<Address>() {}));
    ;

    public static final Event OWNERSHIPTRANSFERRED_EVENT = new Event("OwnershipTransferred", 
            Arrays.<TypeReference<?>>asList(new TypeReference<Address>(true) {}, new TypeReference<Address>(true) {}));
    ;

    public static final Event PAYMENTTOKENUPDATED_EVENT = new Event("PaymentTokenUpdated", 
            Arrays.<TypeReference<?>>asList(new TypeReference<Address>() {}));
    ;

    public static final Event PLATFORMFEESRATEUPDATED_EVENT = new Event("PlatformFeesRateUpdated", 
            Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}));
    ;

    public static final Event RATEUPDATED_EVENT = new Event("RateUpdated", 
            Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}));
    ;

    public static final Event TOKENPURCHASE_EVENT = new Event("TokenPurchase", 
            Arrays.<TypeReference<?>>asList(new TypeReference<Address>(true) {}, new TypeReference<Address>(true) {}, new TypeReference<Uint256>() {}, new TypeReference<Uint256>() {}));
    ;

    public static final Event WHITELISTUPDATED_EVENT = new Event("WhitelistUpdated", 
            Arrays.<TypeReference<?>>asList(new TypeReference<Address>() {}));
    ;

    @Deprecated
    protected CrowdSaleImplementationV1(String contractAddress, Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        super(BINARY, contractAddress, web3j, credentials, gasPrice, gasLimit);
    }

    protected CrowdSaleImplementationV1(String contractAddress, Web3j web3j, Credentials credentials, ContractGasProvider contractGasProvider) {
        super(BINARY, contractAddress, web3j, credentials, contractGasProvider);
    }

    @Deprecated
    protected CrowdSaleImplementationV1(String contractAddress, Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        super(BINARY, contractAddress, web3j, transactionManager, gasPrice, gasLimit);
    }

    protected CrowdSaleImplementationV1(String contractAddress, Web3j web3j, TransactionManager transactionManager, ContractGasProvider contractGasProvider) {
        super(BINARY, contractAddress, web3j, transactionManager, contractGasProvider);
    }

    public List<ContractDeployedEventResponse> getContractDeployedEvents(TransactionReceipt transactionReceipt) {
        List<Contract.EventValuesWithLog> valueList = extractEventParametersWithLog(CONTRACTDEPLOYED_EVENT, transactionReceipt);
        ArrayList<ContractDeployedEventResponse> responses = new ArrayList<ContractDeployedEventResponse>(valueList.size());
        for (Contract.EventValuesWithLog eventValues : valueList) {
            ContractDeployedEventResponse typedResponse = new ContractDeployedEventResponse();
            typedResponse.log = eventValues.getLog();
            typedResponse.openingTime = (BigInteger) eventValues.getNonIndexedValues().get(0).getValue();
            typedResponse.closingTime = (BigInteger) eventValues.getNonIndexedValues().get(1).getValue();
            typedResponse.rate = (BigInteger) eventValues.getNonIndexedValues().get(2).getValue();
            typedResponse.platformFeesRate = (BigInteger) eventValues.getNonIndexedValues().get(3).getValue();
            typedResponse.wallet = (String) eventValues.getNonIndexedValues().get(4).getValue();
            typedResponse.platformFeesWallet = (String) eventValues.getNonIndexedValues().get(5).getValue();
            typedResponse.token = (String) eventValues.getNonIndexedValues().get(6).getValue();
            typedResponse._whitelist = (String) eventValues.getNonIndexedValues().get(7).getValue();
            typedResponse.paymentToken = (String) eventValues.getNonIndexedValues().get(8).getValue();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Flowable<ContractDeployedEventResponse> contractDeployedEventFlowable(EthFilter filter) {
        return web3j.ethLogFlowable(filter).map(new Function<Log, ContractDeployedEventResponse>() {
            @Override
            public ContractDeployedEventResponse apply(Log log) {
                Contract.EventValuesWithLog eventValues = extractEventParametersWithLog(CONTRACTDEPLOYED_EVENT, log);
                ContractDeployedEventResponse typedResponse = new ContractDeployedEventResponse();
                typedResponse.log = log;
                typedResponse.openingTime = (BigInteger) eventValues.getNonIndexedValues().get(0).getValue();
                typedResponse.closingTime = (BigInteger) eventValues.getNonIndexedValues().get(1).getValue();
                typedResponse.rate = (BigInteger) eventValues.getNonIndexedValues().get(2).getValue();
                typedResponse.platformFeesRate = (BigInteger) eventValues.getNonIndexedValues().get(3).getValue();
                typedResponse.wallet = (String) eventValues.getNonIndexedValues().get(4).getValue();
                typedResponse.platformFeesWallet = (String) eventValues.getNonIndexedValues().get(5).getValue();
                typedResponse.token = (String) eventValues.getNonIndexedValues().get(6).getValue();
                typedResponse._whitelist = (String) eventValues.getNonIndexedValues().get(7).getValue();
                typedResponse.paymentToken = (String) eventValues.getNonIndexedValues().get(8).getValue();
                return typedResponse;
            }
        });
    }

    public Flowable<ContractDeployedEventResponse> contractDeployedEventFlowable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(CONTRACTDEPLOYED_EVENT));
        return contractDeployedEventFlowable(filter);
    }

    public List<FastBonusTransferRateUpdatedEventResponse> getFastBonusTransferRateUpdatedEvents(TransactionReceipt transactionReceipt) {
        List<Contract.EventValuesWithLog> valueList = extractEventParametersWithLog(FASTBONUSTRANSFERRATEUPDATED_EVENT, transactionReceipt);
        ArrayList<FastBonusTransferRateUpdatedEventResponse> responses = new ArrayList<FastBonusTransferRateUpdatedEventResponse>(valueList.size());
        for (Contract.EventValuesWithLog eventValues : valueList) {
            FastBonusTransferRateUpdatedEventResponse typedResponse = new FastBonusTransferRateUpdatedEventResponse();
            typedResponse.log = eventValues.getLog();
            typedResponse.rate = (BigInteger) eventValues.getNonIndexedValues().get(0).getValue();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Flowable<FastBonusTransferRateUpdatedEventResponse> fastBonusTransferRateUpdatedEventFlowable(EthFilter filter) {
        return web3j.ethLogFlowable(filter).map(new Function<Log, FastBonusTransferRateUpdatedEventResponse>() {
            @Override
            public FastBonusTransferRateUpdatedEventResponse apply(Log log) {
                Contract.EventValuesWithLog eventValues = extractEventParametersWithLog(FASTBONUSTRANSFERRATEUPDATED_EVENT, log);
                FastBonusTransferRateUpdatedEventResponse typedResponse = new FastBonusTransferRateUpdatedEventResponse();
                typedResponse.log = log;
                typedResponse.rate = (BigInteger) eventValues.getNonIndexedValues().get(0).getValue();
                return typedResponse;
            }
        });
    }

    public Flowable<FastBonusTransferRateUpdatedEventResponse> fastBonusTransferRateUpdatedEventFlowable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(FASTBONUSTRANSFERRATEUPDATED_EVENT));
        return fastBonusTransferRateUpdatedEventFlowable(filter);
    }

    public List<FastBonusWalletUpdatedEventResponse> getFastBonusWalletUpdatedEvents(TransactionReceipt transactionReceipt) {
        List<Contract.EventValuesWithLog> valueList = extractEventParametersWithLog(FASTBONUSWALLETUPDATED_EVENT, transactionReceipt);
        ArrayList<FastBonusWalletUpdatedEventResponse> responses = new ArrayList<FastBonusWalletUpdatedEventResponse>(valueList.size());
        for (Contract.EventValuesWithLog eventValues : valueList) {
            FastBonusWalletUpdatedEventResponse typedResponse = new FastBonusWalletUpdatedEventResponse();
            typedResponse.log = eventValues.getLog();
            typedResponse.fastBonusWallet = (String) eventValues.getNonIndexedValues().get(0).getValue();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Flowable<FastBonusWalletUpdatedEventResponse> fastBonusWalletUpdatedEventFlowable(EthFilter filter) {
        return web3j.ethLogFlowable(filter).map(new Function<Log, FastBonusWalletUpdatedEventResponse>() {
            @Override
            public FastBonusWalletUpdatedEventResponse apply(Log log) {
                Contract.EventValuesWithLog eventValues = extractEventParametersWithLog(FASTBONUSWALLETUPDATED_EVENT, log);
                FastBonusWalletUpdatedEventResponse typedResponse = new FastBonusWalletUpdatedEventResponse();
                typedResponse.log = log;
                typedResponse.fastBonusWallet = (String) eventValues.getNonIndexedValues().get(0).getValue();
                return typedResponse;
            }
        });
    }

    public Flowable<FastBonusWalletUpdatedEventResponse> fastBonusWalletUpdatedEventFlowable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(FASTBONUSWALLETUPDATED_EVENT));
        return fastBonusWalletUpdatedEventFlowable(filter);
    }

    public List<OwnershipTransferredEventResponse> getOwnershipTransferredEvents(TransactionReceipt transactionReceipt) {
        List<Contract.EventValuesWithLog> valueList = extractEventParametersWithLog(OWNERSHIPTRANSFERRED_EVENT, transactionReceipt);
        ArrayList<OwnershipTransferredEventResponse> responses = new ArrayList<OwnershipTransferredEventResponse>(valueList.size());
        for (Contract.EventValuesWithLog eventValues : valueList) {
            OwnershipTransferredEventResponse typedResponse = new OwnershipTransferredEventResponse();
            typedResponse.log = eventValues.getLog();
            typedResponse.previousOwner = (String) eventValues.getIndexedValues().get(0).getValue();
            typedResponse.newOwner = (String) eventValues.getIndexedValues().get(1).getValue();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Flowable<OwnershipTransferredEventResponse> ownershipTransferredEventFlowable(EthFilter filter) {
        return web3j.ethLogFlowable(filter).map(new Function<Log, OwnershipTransferredEventResponse>() {
            @Override
            public OwnershipTransferredEventResponse apply(Log log) {
                Contract.EventValuesWithLog eventValues = extractEventParametersWithLog(OWNERSHIPTRANSFERRED_EVENT, log);
                OwnershipTransferredEventResponse typedResponse = new OwnershipTransferredEventResponse();
                typedResponse.log = log;
                typedResponse.previousOwner = (String) eventValues.getIndexedValues().get(0).getValue();
                typedResponse.newOwner = (String) eventValues.getIndexedValues().get(1).getValue();
                return typedResponse;
            }
        });
    }

    public Flowable<OwnershipTransferredEventResponse> ownershipTransferredEventFlowable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(OWNERSHIPTRANSFERRED_EVENT));
        return ownershipTransferredEventFlowable(filter);
    }

    public List<PaymentTokenUpdatedEventResponse> getPaymentTokenUpdatedEvents(TransactionReceipt transactionReceipt) {
        List<Contract.EventValuesWithLog> valueList = extractEventParametersWithLog(PAYMENTTOKENUPDATED_EVENT, transactionReceipt);
        ArrayList<PaymentTokenUpdatedEventResponse> responses = new ArrayList<PaymentTokenUpdatedEventResponse>(valueList.size());
        for (Contract.EventValuesWithLog eventValues : valueList) {
            PaymentTokenUpdatedEventResponse typedResponse = new PaymentTokenUpdatedEventResponse();
            typedResponse.log = eventValues.getLog();
            typedResponse._paymentToken = (String) eventValues.getNonIndexedValues().get(0).getValue();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Flowable<PaymentTokenUpdatedEventResponse> paymentTokenUpdatedEventFlowable(EthFilter filter) {
        return web3j.ethLogFlowable(filter).map(new Function<Log, PaymentTokenUpdatedEventResponse>() {
            @Override
            public PaymentTokenUpdatedEventResponse apply(Log log) {
                Contract.EventValuesWithLog eventValues = extractEventParametersWithLog(PAYMENTTOKENUPDATED_EVENT, log);
                PaymentTokenUpdatedEventResponse typedResponse = new PaymentTokenUpdatedEventResponse();
                typedResponse.log = log;
                typedResponse._paymentToken = (String) eventValues.getNonIndexedValues().get(0).getValue();
                return typedResponse;
            }
        });
    }

    public Flowable<PaymentTokenUpdatedEventResponse> paymentTokenUpdatedEventFlowable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(PAYMENTTOKENUPDATED_EVENT));
        return paymentTokenUpdatedEventFlowable(filter);
    }

    public List<PlatformFeesRateUpdatedEventResponse> getPlatformFeesRateUpdatedEvents(TransactionReceipt transactionReceipt) {
        List<Contract.EventValuesWithLog> valueList = extractEventParametersWithLog(PLATFORMFEESRATEUPDATED_EVENT, transactionReceipt);
        ArrayList<PlatformFeesRateUpdatedEventResponse> responses = new ArrayList<PlatformFeesRateUpdatedEventResponse>(valueList.size());
        for (Contract.EventValuesWithLog eventValues : valueList) {
            PlatformFeesRateUpdatedEventResponse typedResponse = new PlatformFeesRateUpdatedEventResponse();
            typedResponse.log = eventValues.getLog();
            typedResponse.rate = (BigInteger) eventValues.getNonIndexedValues().get(0).getValue();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Flowable<PlatformFeesRateUpdatedEventResponse> platformFeesRateUpdatedEventFlowable(EthFilter filter) {
        return web3j.ethLogFlowable(filter).map(new Function<Log, PlatformFeesRateUpdatedEventResponse>() {
            @Override
            public PlatformFeesRateUpdatedEventResponse apply(Log log) {
                Contract.EventValuesWithLog eventValues = extractEventParametersWithLog(PLATFORMFEESRATEUPDATED_EVENT, log);
                PlatformFeesRateUpdatedEventResponse typedResponse = new PlatformFeesRateUpdatedEventResponse();
                typedResponse.log = log;
                typedResponse.rate = (BigInteger) eventValues.getNonIndexedValues().get(0).getValue();
                return typedResponse;
            }
        });
    }

    public Flowable<PlatformFeesRateUpdatedEventResponse> platformFeesRateUpdatedEventFlowable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(PLATFORMFEESRATEUPDATED_EVENT));
        return platformFeesRateUpdatedEventFlowable(filter);
    }

    public List<RateUpdatedEventResponse> getRateUpdatedEvents(TransactionReceipt transactionReceipt) {
        List<Contract.EventValuesWithLog> valueList = extractEventParametersWithLog(RATEUPDATED_EVENT, transactionReceipt);
        ArrayList<RateUpdatedEventResponse> responses = new ArrayList<RateUpdatedEventResponse>(valueList.size());
        for (Contract.EventValuesWithLog eventValues : valueList) {
            RateUpdatedEventResponse typedResponse = new RateUpdatedEventResponse();
            typedResponse.log = eventValues.getLog();
            typedResponse.rate = (BigInteger) eventValues.getNonIndexedValues().get(0).getValue();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Flowable<RateUpdatedEventResponse> rateUpdatedEventFlowable(EthFilter filter) {
        return web3j.ethLogFlowable(filter).map(new Function<Log, RateUpdatedEventResponse>() {
            @Override
            public RateUpdatedEventResponse apply(Log log) {
                Contract.EventValuesWithLog eventValues = extractEventParametersWithLog(RATEUPDATED_EVENT, log);
                RateUpdatedEventResponse typedResponse = new RateUpdatedEventResponse();
                typedResponse.log = log;
                typedResponse.rate = (BigInteger) eventValues.getNonIndexedValues().get(0).getValue();
                return typedResponse;
            }
        });
    }

    public Flowable<RateUpdatedEventResponse> rateUpdatedEventFlowable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(RATEUPDATED_EVENT));
        return rateUpdatedEventFlowable(filter);
    }

    public List<TokenPurchaseEventResponse> getTokenPurchaseEvents(TransactionReceipt transactionReceipt) {
        List<Contract.EventValuesWithLog> valueList = extractEventParametersWithLog(TOKENPURCHASE_EVENT, transactionReceipt);
        ArrayList<TokenPurchaseEventResponse> responses = new ArrayList<TokenPurchaseEventResponse>(valueList.size());
        for (Contract.EventValuesWithLog eventValues : valueList) {
            TokenPurchaseEventResponse typedResponse = new TokenPurchaseEventResponse();
            typedResponse.log = eventValues.getLog();
            typedResponse.purchaser = (String) eventValues.getIndexedValues().get(0).getValue();
            typedResponse.beneficiary = (String) eventValues.getIndexedValues().get(1).getValue();
            typedResponse.value = (BigInteger) eventValues.getNonIndexedValues().get(0).getValue();
            typedResponse.amount = (BigInteger) eventValues.getNonIndexedValues().get(1).getValue();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Flowable<TokenPurchaseEventResponse> tokenPurchaseEventFlowable(EthFilter filter) {
        return web3j.ethLogFlowable(filter).map(new Function<Log, TokenPurchaseEventResponse>() {
            @Override
            public TokenPurchaseEventResponse apply(Log log) {
                Contract.EventValuesWithLog eventValues = extractEventParametersWithLog(TOKENPURCHASE_EVENT, log);
                TokenPurchaseEventResponse typedResponse = new TokenPurchaseEventResponse();
                typedResponse.log = log;
                typedResponse.purchaser = (String) eventValues.getIndexedValues().get(0).getValue();
                typedResponse.beneficiary = (String) eventValues.getIndexedValues().get(1).getValue();
                typedResponse.value = (BigInteger) eventValues.getNonIndexedValues().get(0).getValue();
                typedResponse.amount = (BigInteger) eventValues.getNonIndexedValues().get(1).getValue();
                return typedResponse;
            }
        });
    }

    public Flowable<TokenPurchaseEventResponse> tokenPurchaseEventFlowable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(TOKENPURCHASE_EVENT));
        return tokenPurchaseEventFlowable(filter);
    }

    public List<WhitelistUpdatedEventResponse> getWhitelistUpdatedEvents(TransactionReceipt transactionReceipt) {
        List<Contract.EventValuesWithLog> valueList = extractEventParametersWithLog(WHITELISTUPDATED_EVENT, transactionReceipt);
        ArrayList<WhitelistUpdatedEventResponse> responses = new ArrayList<WhitelistUpdatedEventResponse>(valueList.size());
        for (Contract.EventValuesWithLog eventValues : valueList) {
            WhitelistUpdatedEventResponse typedResponse = new WhitelistUpdatedEventResponse();
            typedResponse.log = eventValues.getLog();
            typedResponse._whitelist = (String) eventValues.getNonIndexedValues().get(0).getValue();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Flowable<WhitelistUpdatedEventResponse> whitelistUpdatedEventFlowable(EthFilter filter) {
        return web3j.ethLogFlowable(filter).map(new Function<Log, WhitelistUpdatedEventResponse>() {
            @Override
            public WhitelistUpdatedEventResponse apply(Log log) {
                Contract.EventValuesWithLog eventValues = extractEventParametersWithLog(WHITELISTUPDATED_EVENT, log);
                WhitelistUpdatedEventResponse typedResponse = new WhitelistUpdatedEventResponse();
                typedResponse.log = log;
                typedResponse._whitelist = (String) eventValues.getNonIndexedValues().get(0).getValue();
                return typedResponse;
            }
        });
    }

    public Flowable<WhitelistUpdatedEventResponse> whitelistUpdatedEventFlowable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(WHITELISTUPDATED_EVENT));
        return whitelistUpdatedEventFlowable(filter);
    }

    public RemoteFunctionCall<TransactionReceipt> buyTokens(String _beneficiary, BigInteger _weiAmount) {
        final org.web3j.abi.datatypes.Function function = new org.web3j.abi.datatypes.Function(
                FUNC_BUYTOKENS, 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.Address(160, _beneficiary), 
                new org.web3j.abi.datatypes.generated.Uint256(_weiAmount)), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteFunctionCall<BigInteger> closingTime() {
        final org.web3j.abi.datatypes.Function function = new org.web3j.abi.datatypes.Function(FUNC_CLOSINGTIME, 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}));
        return executeRemoteCallSingleValueReturn(function, BigInteger.class);
    }

    public RemoteFunctionCall<BigInteger> fastBonusTransferRate() {
        final org.web3j.abi.datatypes.Function function = new org.web3j.abi.datatypes.Function(FUNC_FASTBONUSTRANSFERRATE, 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}));
        return executeRemoteCallSingleValueReturn(function, BigInteger.class);
    }

    public RemoteFunctionCall<String> fastBonusWallet() {
        final org.web3j.abi.datatypes.Function function = new org.web3j.abi.datatypes.Function(FUNC_FASTBONUSWALLET, 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Address>() {}));
        return executeRemoteCallSingleValueReturn(function, String.class);
    }

    public RemoteFunctionCall<BigInteger> getFastBonusAmountToSubtract(BigInteger _weiAmount) {
        final org.web3j.abi.datatypes.Function function = new org.web3j.abi.datatypes.Function(FUNC_GETFASTBONUSAMOUNTTOSUBTRACT, 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.generated.Uint256(_weiAmount)), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}));
        return executeRemoteCallSingleValueReturn(function, BigInteger.class);
    }

    public RemoteFunctionCall<BigInteger> getPlatformFeesAmount(BigInteger _weiAmount) {
        final org.web3j.abi.datatypes.Function function = new org.web3j.abi.datatypes.Function(FUNC_GETPLATFORMFEESAMOUNT, 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.generated.Uint256(_weiAmount)), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}));
        return executeRemoteCallSingleValueReturn(function, BigInteger.class);
    }

    public RemoteFunctionCall<BigInteger> getTokenAmount(BigInteger _weiAmount) {
        final org.web3j.abi.datatypes.Function function = new org.web3j.abi.datatypes.Function(FUNC_GETTOKENAMOUNT, 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.generated.Uint256(_weiAmount)), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}));
        return executeRemoteCallSingleValueReturn(function, BigInteger.class);
    }

    public RemoteFunctionCall<Tuple2<BigInteger, BigInteger>> getTokenToReceive(BigInteger _weiAmount) {
        final org.web3j.abi.datatypes.Function function = new org.web3j.abi.datatypes.Function(FUNC_GETTOKENTORECEIVE, 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.generated.Uint256(_weiAmount)), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}, new TypeReference<Uint256>() {}));
        return new RemoteFunctionCall<Tuple2<BigInteger, BigInteger>>(function,
                new Callable<Tuple2<BigInteger, BigInteger>>() {
                    @Override
                    public Tuple2<BigInteger, BigInteger> call() throws Exception {
                        List<Type> results = executeCallMultipleValueReturn(function);
                        return new Tuple2<BigInteger, BigInteger>(
                                (BigInteger) results.get(0).getValue(), 
                                (BigInteger) results.get(1).getValue());
                    }
                });
    }

    public RemoteFunctionCall<Boolean> hasClosed() {
        final org.web3j.abi.datatypes.Function function = new org.web3j.abi.datatypes.Function(FUNC_HASCLOSED, 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Bool>() {}));
        return executeRemoteCallSingleValueReturn(function, Boolean.class);
    }

    public RemoteFunctionCall<TransactionReceipt> initialize(BigInteger _openingTime, BigInteger _closingTime, BigInteger _rate, BigInteger _platformFeesRate, String _wallet, String _platformFeesWallet, String _token, String _whitelist, String _paymentToken, String owner) {
        final org.web3j.abi.datatypes.Function function = new org.web3j.abi.datatypes.Function(
                FUNC_INITIALIZE, 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.generated.Uint256(_openingTime), 
                new org.web3j.abi.datatypes.generated.Uint256(_closingTime), 
                new org.web3j.abi.datatypes.generated.Uint256(_rate), 
                new org.web3j.abi.datatypes.generated.Uint256(_platformFeesRate), 
                new org.web3j.abi.datatypes.Address(160, _wallet), 
                new org.web3j.abi.datatypes.Address(160, _platformFeesWallet), 
                new org.web3j.abi.datatypes.Address(160, _token), 
                new org.web3j.abi.datatypes.Address(160, _whitelist), 
                new org.web3j.abi.datatypes.Address(160, _paymentToken), 
                new org.web3j.abi.datatypes.Address(160, owner)), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteFunctionCall<BigInteger> openingTime() {
        final org.web3j.abi.datatypes.Function function = new org.web3j.abi.datatypes.Function(FUNC_OPENINGTIME, 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}));
        return executeRemoteCallSingleValueReturn(function, BigInteger.class);
    }

    public RemoteFunctionCall<String> owner() {
        final org.web3j.abi.datatypes.Function function = new org.web3j.abi.datatypes.Function(FUNC_OWNER, 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Address>() {}));
        return executeRemoteCallSingleValueReturn(function, String.class);
    }

    public RemoteFunctionCall<String> paymentToken() {
        final org.web3j.abi.datatypes.Function function = new org.web3j.abi.datatypes.Function(FUNC_PAYMENTTOKEN, 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Address>() {}));
        return executeRemoteCallSingleValueReturn(function, String.class);
    }

    public RemoteFunctionCall<BigInteger> platformFeesCollected() {
        final org.web3j.abi.datatypes.Function function = new org.web3j.abi.datatypes.Function(FUNC_PLATFORMFEESCOLLECTED, 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}));
        return executeRemoteCallSingleValueReturn(function, BigInteger.class);
    }

    public RemoteFunctionCall<BigInteger> platformFeesRate() {
        final org.web3j.abi.datatypes.Function function = new org.web3j.abi.datatypes.Function(FUNC_PLATFORMFEESRATE, 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}));
        return executeRemoteCallSingleValueReturn(function, BigInteger.class);
    }

    public RemoteFunctionCall<String> platformFeesWallet() {
        final org.web3j.abi.datatypes.Function function = new org.web3j.abi.datatypes.Function(FUNC_PLATFORMFEESWALLET, 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Address>() {}));
        return executeRemoteCallSingleValueReturn(function, String.class);
    }

    public RemoteFunctionCall<BigInteger> rate() {
        final org.web3j.abi.datatypes.Function function = new org.web3j.abi.datatypes.Function(FUNC_RATE, 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}));
        return executeRemoteCallSingleValueReturn(function, BigInteger.class);
    }

    public RemoteFunctionCall<TransactionReceipt> renounceOwnership() {
        final org.web3j.abi.datatypes.Function function = new org.web3j.abi.datatypes.Function(
                FUNC_RENOUNCEOWNERSHIP, 
                Arrays.<Type>asList(), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteFunctionCall<TransactionReceipt> setPaymentToken(String _paymentToken) {
        final org.web3j.abi.datatypes.Function function = new org.web3j.abi.datatypes.Function(
                FUNC_SETPAYMENTTOKEN, 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.Address(160, _paymentToken)), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteFunctionCall<TransactionReceipt> setWhitelistContract(String _whitelist) {
        final org.web3j.abi.datatypes.Function function = new org.web3j.abi.datatypes.Function(
                FUNC_SETWHITELISTCONTRACT, 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.Address(160, _whitelist)), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteFunctionCall<String> token() {
        final org.web3j.abi.datatypes.Function function = new org.web3j.abi.datatypes.Function(FUNC_TOKEN, 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Address>() {}));
        return executeRemoteCallSingleValueReturn(function, String.class);
    }

    public RemoteFunctionCall<TransactionReceipt> transferOwnership(String newOwner) {
        final org.web3j.abi.datatypes.Function function = new org.web3j.abi.datatypes.Function(
                FUNC_TRANSFEROWNERSHIP, 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.Address(160, newOwner)), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteFunctionCall<TransactionReceipt> updateFastBonusTransferRate(BigInteger _fastBonusTransferRate) {
        final org.web3j.abi.datatypes.Function function = new org.web3j.abi.datatypes.Function(
                FUNC_UPDATEFASTBONUSTRANSFERRATE, 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.generated.Uint256(_fastBonusTransferRate)), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteFunctionCall<TransactionReceipt> updateFastBonusWallet(String _fastBonusWallet) {
        final org.web3j.abi.datatypes.Function function = new org.web3j.abi.datatypes.Function(
                FUNC_UPDATEFASTBONUSWALLET, 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.Address(160, _fastBonusWallet)), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteFunctionCall<TransactionReceipt> updatePlatformFeesRate(BigInteger _platformFeesRate) {
        final org.web3j.abi.datatypes.Function function = new org.web3j.abi.datatypes.Function(
                FUNC_UPDATEPLATFORMFEESRATE, 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.generated.Uint256(_platformFeesRate)), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteFunctionCall<TransactionReceipt> updateRate(BigInteger _rate) {
        final org.web3j.abi.datatypes.Function function = new org.web3j.abi.datatypes.Function(
                FUNC_UPDATERATE, 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.generated.Uint256(_rate)), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteFunctionCall<String> wallet() {
        final org.web3j.abi.datatypes.Function function = new org.web3j.abi.datatypes.Function(FUNC_WALLET, 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Address>() {}));
        return executeRemoteCallSingleValueReturn(function, String.class);
    }

    public RemoteFunctionCall<BigInteger> weiRaised() {
        final org.web3j.abi.datatypes.Function function = new org.web3j.abi.datatypes.Function(FUNC_WEIRAISED, 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}));
        return executeRemoteCallSingleValueReturn(function, BigInteger.class);
    }

    public RemoteFunctionCall<String> whitelist() {
        final org.web3j.abi.datatypes.Function function = new org.web3j.abi.datatypes.Function(FUNC_WHITELIST, 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Address>() {}));
        return executeRemoteCallSingleValueReturn(function, String.class);
    }

    @Deprecated
    public static CrowdSaleImplementationV1 load(String contractAddress, Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        return new CrowdSaleImplementationV1(contractAddress, web3j, credentials, gasPrice, gasLimit);
    }

    @Deprecated
    public static CrowdSaleImplementationV1 load(String contractAddress, Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        return new CrowdSaleImplementationV1(contractAddress, web3j, transactionManager, gasPrice, gasLimit);
    }

    public static CrowdSaleImplementationV1 load(String contractAddress, Web3j web3j, Credentials credentials, ContractGasProvider contractGasProvider) {
        return new CrowdSaleImplementationV1(contractAddress, web3j, credentials, contractGasProvider);
    }

    public static CrowdSaleImplementationV1 load(String contractAddress, Web3j web3j, TransactionManager transactionManager, ContractGasProvider contractGasProvider) {
        return new CrowdSaleImplementationV1(contractAddress, web3j, transactionManager, contractGasProvider);
    }

    public static RemoteCall<CrowdSaleImplementationV1> deploy(Web3j web3j, Credentials credentials, ContractGasProvider contractGasProvider, BigInteger _openingTime, BigInteger _closingTime, BigInteger _rate, BigInteger _platformFeesRate, String _wallet, String _platformFeesWallet, String _token, String _whitelist, String _paymentToken) {
        String encodedConstructor = FunctionEncoder.encodeConstructor(Arrays.<Type>asList(new org.web3j.abi.datatypes.generated.Uint256(_openingTime), 
                new org.web3j.abi.datatypes.generated.Uint256(_closingTime), 
                new org.web3j.abi.datatypes.generated.Uint256(_rate), 
                new org.web3j.abi.datatypes.generated.Uint256(_platformFeesRate), 
                new org.web3j.abi.datatypes.Address(160, _wallet), 
                new org.web3j.abi.datatypes.Address(160, _platformFeesWallet), 
                new org.web3j.abi.datatypes.Address(160, _token), 
                new org.web3j.abi.datatypes.Address(160, _whitelist), 
                new org.web3j.abi.datatypes.Address(160, _paymentToken)));
        return deployRemoteCall(CrowdSaleImplementationV1.class, web3j, credentials, contractGasProvider, BINARY, encodedConstructor);
    }

    public static RemoteCall<CrowdSaleImplementationV1> deploy(Web3j web3j, TransactionManager transactionManager, ContractGasProvider contractGasProvider, BigInteger _openingTime, BigInteger _closingTime, BigInteger _rate, BigInteger _platformFeesRate, String _wallet, String _platformFeesWallet, String _token, String _whitelist, String _paymentToken) {
        String encodedConstructor = FunctionEncoder.encodeConstructor(Arrays.<Type>asList(new org.web3j.abi.datatypes.generated.Uint256(_openingTime), 
                new org.web3j.abi.datatypes.generated.Uint256(_closingTime), 
                new org.web3j.abi.datatypes.generated.Uint256(_rate), 
                new org.web3j.abi.datatypes.generated.Uint256(_platformFeesRate), 
                new org.web3j.abi.datatypes.Address(160, _wallet), 
                new org.web3j.abi.datatypes.Address(160, _platformFeesWallet), 
                new org.web3j.abi.datatypes.Address(160, _token), 
                new org.web3j.abi.datatypes.Address(160, _whitelist), 
                new org.web3j.abi.datatypes.Address(160, _paymentToken)));
        return deployRemoteCall(CrowdSaleImplementationV1.class, web3j, transactionManager, contractGasProvider, BINARY, encodedConstructor);
    }

    @Deprecated
    public static RemoteCall<CrowdSaleImplementationV1> deploy(Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit, BigInteger _openingTime, BigInteger _closingTime, BigInteger _rate, BigInteger _platformFeesRate, String _wallet, String _platformFeesWallet, String _token, String _whitelist, String _paymentToken) {
        String encodedConstructor = FunctionEncoder.encodeConstructor(Arrays.<Type>asList(new org.web3j.abi.datatypes.generated.Uint256(_openingTime), 
                new org.web3j.abi.datatypes.generated.Uint256(_closingTime), 
                new org.web3j.abi.datatypes.generated.Uint256(_rate), 
                new org.web3j.abi.datatypes.generated.Uint256(_platformFeesRate), 
                new org.web3j.abi.datatypes.Address(160, _wallet), 
                new org.web3j.abi.datatypes.Address(160, _platformFeesWallet), 
                new org.web3j.abi.datatypes.Address(160, _token), 
                new org.web3j.abi.datatypes.Address(160, _whitelist), 
                new org.web3j.abi.datatypes.Address(160, _paymentToken)));
        return deployRemoteCall(CrowdSaleImplementationV1.class, web3j, credentials, gasPrice, gasLimit, BINARY, encodedConstructor);
    }

    @Deprecated
    public static RemoteCall<CrowdSaleImplementationV1> deploy(Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit, BigInteger _openingTime, BigInteger _closingTime, BigInteger _rate, BigInteger _platformFeesRate, String _wallet, String _platformFeesWallet, String _token, String _whitelist, String _paymentToken) {
        String encodedConstructor = FunctionEncoder.encodeConstructor(Arrays.<Type>asList(new org.web3j.abi.datatypes.generated.Uint256(_openingTime), 
                new org.web3j.abi.datatypes.generated.Uint256(_closingTime), 
                new org.web3j.abi.datatypes.generated.Uint256(_rate), 
                new org.web3j.abi.datatypes.generated.Uint256(_platformFeesRate), 
                new org.web3j.abi.datatypes.Address(160, _wallet), 
                new org.web3j.abi.datatypes.Address(160, _platformFeesWallet), 
                new org.web3j.abi.datatypes.Address(160, _token), 
                new org.web3j.abi.datatypes.Address(160, _whitelist), 
                new org.web3j.abi.datatypes.Address(160, _paymentToken)));
        return deployRemoteCall(CrowdSaleImplementationV1.class, web3j, transactionManager, gasPrice, gasLimit, BINARY, encodedConstructor);
    }

    public static class ContractDeployedEventResponse extends BaseEventResponse {
        public BigInteger openingTime;

        public BigInteger closingTime;

        public BigInteger rate;

        public BigInteger platformFeesRate;

        public String wallet;

        public String platformFeesWallet;

        public String token;

        public String _whitelist;

        public String paymentToken;
    }

    public static class FastBonusTransferRateUpdatedEventResponse extends BaseEventResponse {
        public BigInteger rate;
    }

    public static class FastBonusWalletUpdatedEventResponse extends BaseEventResponse {
        public String fastBonusWallet;
    }

    public static class OwnershipTransferredEventResponse extends BaseEventResponse {
        public String previousOwner;

        public String newOwner;
    }

    public static class PaymentTokenUpdatedEventResponse extends BaseEventResponse {
        public String _paymentToken;
    }

    public static class PlatformFeesRateUpdatedEventResponse extends BaseEventResponse {
        public BigInteger rate;
    }

    public static class RateUpdatedEventResponse extends BaseEventResponse {
        public BigInteger rate;
    }

    public static class TokenPurchaseEventResponse extends BaseEventResponse {
        public String purchaser;

        public String beneficiary;

        public BigInteger value;

        public BigInteger amount;
    }

    public static class WhitelistUpdatedEventResponse extends BaseEventResponse {
        public String _whitelist;
    }
}
