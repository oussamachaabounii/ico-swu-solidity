// File: openzeppelin-solidity\contracts\math\SafeMath.sol

// SPDX-License-Identifier: MIT

pragma solidity ^0.6.0;

/**
 * @dev Wrappers over Solidity's arithmetic operations with added overflow
 * checks.
 *
 * Arithmetic operations in Solidity wrap on overflow. This can easily result
 * in bugs, because programmers usually assume that an overflow raises an
 * error, which is the standard behavior in high level programming languages.
 * `SafeMath` restores this intuition by reverting the transaction when an
 * operation overflows.
 *
 * Using this library instead of the unchecked operations eliminates an entire
 * class of bugs, so it's recommended to use it always.
 */
library SafeMath {
    /**
     * @dev Returns the addition of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity's `+` operator.
     *
     * Requirements:
     *
     * - Addition cannot overflow.
     */
    function add(uint256 a, uint256 b) internal pure returns (uint256) {
        uint256 c = a + b;
        require(c >= a, "SafeMath: addition overflow");

        return c;
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting on
     * overflow (when the result is negative).
     *
     * Counterpart to Solidity's `-` operator.
     *
     * Requirements:
     *
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b) internal pure returns (uint256) {
        return sub(a, b, "SafeMath: subtraction overflow");
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting with custom message on
     * overflow (when the result is negative).
     *
     * Counterpart to Solidity's `-` operator.
     *
     * Requirements:
     *
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b <= a, errorMessage);
        uint256 c = a - b;

        return c;
    }

    /**
     * @dev Returns the multiplication of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity's `*` operator.
     *
     * Requirements:
     *
     * - Multiplication cannot overflow.
     */
    function mul(uint256 a, uint256 b) internal pure returns (uint256) {
        // Gas optimization: this is cheaper than requiring 'a' not being zero, but the
        // benefit is lost if 'b' is also tested.
        // See: https://github.com/OpenZeppelin/openzeppelin-contracts/pull/522
        if (a == 0) {
            return 0;
        }

        uint256 c = a * b;
        require(c / a == b, "SafeMath: multiplication overflow");

        return c;
    }

    /**
     * @dev Returns the integer division of two unsigned integers. Reverts on
     * division by zero. The result is rounded towards zero.
     *
     * Counterpart to Solidity's `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b) internal pure returns (uint256) {
        return div(a, b, "SafeMath: division by zero");
    }

    /**
     * @dev Returns the integer division of two unsigned integers. Reverts with custom message on
     * division by zero. The result is rounded towards zero.
     *
     * Counterpart to Solidity's `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b > 0, errorMessage);
        uint256 c = a / b;
        // assert(a == b * c + a % b); // There is no case in which this doesn't hold

        return c;
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * Reverts when dividing by zero.
     *
     * Counterpart to Solidity's `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b) internal pure returns (uint256) {
        return mod(a, b, "SafeMath: modulo by zero");
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * Reverts with custom message when dividing by zero.
     *
     * Counterpart to Solidity's `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b != 0, errorMessage);
        return a % b;
    }
}

// File: openzeppelin-solidity\contracts\GSN\Context.sol

// SPDX-License-Identifier: MIT

pragma solidity ^0.6.0;

/*
 * @dev Provides information about the current execution context, including the
 * sender of the transaction and its data. While these are generally available
 * via msg.sender and msg.data, they should not be accessed in such a direct
 * manner, since when dealing with GSN meta-transactions the account sending and
 * paying for execution may not be the actual sender (as far as an application
 * is concerned).
 *
 * This contract is only required for intermediate, library-like contracts.
 */
abstract contract Context {
    function _msgSender() internal view virtual returns (address payable) {
        return msg.sender;
    }

    function _msgData() internal view virtual returns (bytes memory) {
        this; // silence state mutability warning without generating bytecode - see https://github.com/ethereum/solidity/issues/2691
        return msg.data;
    }
}

// File: contracts\bep20\IBEP20.sol

// SPDX-License-Identifier: MIT

pragma solidity >=0.6.0;

interface IERC20 {
    /**
     * @dev Returns the amount of tokens in existence.
     */
    function totalSupply() external view returns (uint256);

    /**
     * @dev Returns the token decimals.
     */
    function decimals() external view returns (uint8);

    /**
     * @dev Returns the token symbol.
     */
    function symbol() external view returns (string memory);

    /**
    * @dev Returns the token name.
    */
    function name() external view returns (string memory);

    /**
     * @dev Returns the bep token owner.
     */
    function getOwner() external view returns (address);

    /**
     * @dev Returns the amount of tokens owned by `account`.
     */
    function balanceOf(address account) external view returns (uint256);

    /**
     * @dev Moves `amount` tokens from the caller's account to `recipient`.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transfer(address recipient, uint256 amount) external returns (bool);

    /**
     * @dev Returns the remaining number of tokens that `spender` will be
     * allowed to spend on behalf of `owner` through {transferFrom}. This is
     * zero by default.
     *
     * This value changes when {approve} or {transferFrom} are called.
     */
    function allowance(address _owner, address spender) external view returns (uint256);

    /**
     * @dev Sets `amount` as the allowance of `spender` over the caller's tokens.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * IMPORTANT: Beware that changing an allowance with this method brings the risk
     * that someone may use both the old and the new allowance by unfortunate
     * transaction ordering. One possible solution to mitigate this race
     * condition is to first reduce the spender's allowance to 0 and set the
     * desired value afterwards:
     * https://github.com/ethereum/EIPs/issues/20#issuecomment-263524729
     *
     * Emits an {Approval} event.
     */
    function approve(address spender, uint256 amount) external returns (bool);

    /**
     * @dev Moves `amount` tokens from `sender` to `recipient` using the
     * allowance mechanism. `amount` is then deducted from the caller's
     * allowance.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transferFrom(address sender, address recipient, uint256 amount) external returns (bool);

    /**
     * @dev Emitted when `value` tokens are moved from one account (`from`) to
     * another (`to`).
     *
     * Note that `value` may be zero.
     */
    event Transfer(address indexed from, address indexed to, uint256 value);

    /**
     * @dev Emitted when the allowance of a `spender` for an `owner` is set by
     * a call to {approve}. `value` is the new allowance.
     */
    event Approval(address indexed owner, address indexed spender, uint256 value);
}

// File: contracts\modules\transfer\ITransferVerificationModule.sol

// SPDX-License-Identifier: MIT
pragma solidity >=0.6.0;
/**
* @notice module to check if user can do transfert or not
 */
interface ITransferVerificationModule{
    function isAutorized(address _address) external view returns (bool);
}

// File: contracts\crowdsale\ERC20PaymentCrowdsale.sol

//SPDX-License-Identifier: MIT
pragma solidity >=0.6.0;





/**
 * @title Crowdsale
 * @dev Crowdsale is a base contract for managing a token crowdsale,
 * allowing investors to purchase tokens with any ERC20 contract. This contract implements
 * such functionality in its most fundamental form and can be extended to provide additional
 * functionality and/or custom behavior.
 * The external interface represents the basic interface for purchasing tokens, and conform
 * the base architecture for crowdsales. They are *not* intended to be modified / overriden.
 * The internal interface conforms the extensible and modifiable surface of crowdsales. Override
 * the methods to add functionality. Consider using 'super' where appropiate to concatenate
 * behavior.
 */

contract ERC20PaymentCrowdsale is Context {
    using SafeMath for uint256;

    uint256 public openingTime;
    uint256 public closingTime;
    //accepted payment token
    IERC20 public paymentToken;
    // The token being sold
    IERC20 public token;

    // Address where funds are collected
    address payable public wallet;
    // Address where platform fees are collected
    address payable public platformFeesWallet;

    // How many token units a buyer gets per wei
    uint256 public rate;
    
    // How many token units platform gets per wei
    uint256 public platformFeesRate;

    // Amount of wei raised
    uint256 public weiRaised;

    // Amount of wei platform fees collected
    uint256 public platformFeesCollected;

    //Contract owner
    address internal _owner;

    /**
     * Event for token purchase logging
     * @param purchaser who paid for the tokens
     * @param beneficiary who got the tokens
     * @param value weis paid for purchase
     * @param amount amount of tokens purchased
     */
    event TokenPurchase(
        address indexed purchaser,
        address indexed beneficiary,
        uint256 value,
        uint256 amount
    );

    event OwnershipTransferred(
        address indexed previousOwner,
        address indexed newOwner
    );
    event RateUpdated(uint256 rate);
    event PlatformFeesRateUpdated(uint256 rate);

    ITransferVerificationModule public whitelist;

    event ContractDeployed(
        uint256 openingTime,
        uint256 closingTime,
        uint256 rate,
        uint256 platformFeesRate,
        address wallet,
        address platformFeesWallet,
        IERC20 token,
        ITransferVerificationModule _whitelist,
        IERC20 paymentToken
    );

    event WhitelistUpdated(ITransferVerificationModule _whitelist);
    event PaymentTokenUpdated(IERC20 _paymentToken);

    /**
     * @dev Reverts if not in crowdsale time range.
     */
    modifier onlyWhileOpen {
        // solium-disable-next-line security/no-block-members
        require(
            block.timestamp >= openingTime && block.timestamp <= closingTime,
            "TimedCrowdsale: ICO is not opened..."
        );
        _;
    }

    /**
     * @dev Checks whether the period in which the crowdsale is open has already elapsed.
     * @return Whether crowdsale period has elapsed
     */
    function hasClosed() public view returns (bool) {
        // solium-disable-next-line security/no-block-members
        return block.timestamp > closingTime;
    }

    /**
     *@param _openingTime crowdsate opening Epoch timestamp
     *@param _closingTime crowdsate closing Epoch timestamp
     *@param _rate is represented with real percentage x (10^6)
     *@param _platformFeesRate is represented with real percentage x (10^6)
     *@param _wallet forward collected tokens to this address
     *@param _platformFeesWallet forward collected platform fees tokens to this address
     *@param _token tokens for sale
     *@param _whitelist whitelist contract address
     *@param _paymentToken erc20 token used for purchase
     */
    constructor(
        uint256 _openingTime,
        uint256 _closingTime,
        uint256 _rate,
        uint256 _platformFeesRate,
        address payable _wallet,
        address payable _platformFeesWallet,
        IERC20 _token,
        ITransferVerificationModule _whitelist,
        IERC20 _paymentToken
    ) public {
        require(_rate > 0, "Crowdsale: Invalid rate...");
        require(
            _wallet != address(0),
            "Crowdsale: Invalid wallet, zero address..."
        );
        require(
            _platformFeesWallet != address(0),
            "Crowdsale: Invalid platform wallet fees, zero address..."
        );
        require(
            address(_token) != address(0),
            "Crowdsale: Invalid ERC20 Token, zero address..."
        );
        // solium-disable-next-line security/no-block-members
        require(
            _openingTime >= block.timestamp,
            "TimedCrowdsale: Invalid openingTime..."
        );
        require(
            _closingTime >= _openingTime,
            "TimedCrowdsale: Invalid closingTime..."
        );

        _owner = msg.sender;
        openingTime = _openingTime;
        closingTime = _closingTime;
        rate = _rate;
        platformFeesRate = _platformFeesRate;
        wallet = _wallet;
        platformFeesWallet = _platformFeesWallet;
        token = _token;
        _setWhitelistContract(_whitelist);
        _setPaymentToken(_paymentToken);

        emit ContractDeployed(
            _openingTime,
            _closingTime,
            _rate,
            _platformFeesRate,
            _wallet,
            _platformFeesWallet,
            _token,
            _whitelist,
            _paymentToken
        );
    }

    function setWhitelistContract(ITransferVerificationModule _whitelist)
        external
        onlyOwner
    {
        _setWhitelistContract(_whitelist);
    }

    function _setWhitelistContract(ITransferVerificationModule _whitelist)
        internal
    {
        whitelist = _whitelist;
        emit WhitelistUpdated(_whitelist);
    }

    function setPaymentToken(IERC20 _paymentToken) external onlyOwner {
        _setPaymentToken(_paymentToken);
    }

    function _setPaymentToken(IERC20 _paymentToken) internal {
        paymentToken = _paymentToken;
        emit PaymentTokenUpdated(_paymentToken);
    }

    /**
     * @param _beneficiary Address performing the token purchase
     * @param _weiAmount weiAmount to pay
     */
    function buyTokens(address _beneficiary, uint256 _weiAmount) public {
        uint256 weiAmount = _weiAmount;
        _preValidatePurchase(_beneficiary, weiAmount);

        paymentToken.transferFrom(msg.sender, address(this), _weiAmount);

        // calculate token amount to be sent
        uint256 tokens = _getTokenAmount(weiAmount);

        // calculate sale transaction raised amount 

        uint256 transactionPlatformFeesCollected = _getPlatformFeesAmount(weiAmount);
        platformFeesCollected = platformFeesCollected.add(transactionPlatformFeesCollected);
        uint256 transactionWeiRaised = weiAmount.sub(transactionPlatformFeesCollected);

        // update state
        weiRaised = weiRaised.add(transactionWeiRaised);

        _processPurchase(_beneficiary, tokens);
        emit TokenPurchase(msg.sender, _beneficiary, weiAmount, tokens);

        _updatePurchasingState(_beneficiary, weiAmount);

        _forwardFunds(_weiAmount);
        _postValidatePurchase(_beneficiary, weiAmount);
    }

    /**
     * @dev Extend parent behavior requiring beneficiary to be in whitelist.
     * @param _beneficiary Token beneficiary
     * @param _weiAmount Amount of wei contributed
     */
    function _preValidatePurchase(address _beneficiary, uint256 _weiAmount)
        virtual
        internal
        onlyWhileOpen
    {
        require(whitelist.isAutorized(_beneficiary));
        require(
            _beneficiary != address(0),
            "Crowdsale: Invalid beneficiary..."
        );
        require(_weiAmount != 0, "Crowdsale: Invalid weiAmount...");
        require(
            paymentToken.allowance(msg.sender, address(this)) >= _weiAmount,
            "Payment token allowance too low"
        );

        uint256 tokens = _getTokenAmount(_weiAmount);
        require(
            tokens <= token.balanceOf(address(this)),
            "Tokens to receive exceeds balance"
        );
    }

    /**
     * @dev Validation of an executed purchase.
     *Observe state and use revert statements to undo rollback when valid conditions are not met.
     * @param _beneficiary Address performing the token purchase
     * @param _weiAmount Value in wei involved in the purchase
     */
    function _postValidatePurchase(address _beneficiary, uint256 _weiAmount)
        internal
    {
        // optional override
    }

    /**
     * @dev Source of tokens. Override this method to modify the way in which the crowdsale
     * ultimately gets and sends its tokens.
     * @param _beneficiary Address performing the token purchase
     * @param _tokenAmount Number of tokens to be emitted
     */
    function _deliverTokens(address _beneficiary, uint256 _tokenAmount)
        virtual
        internal
    {
        token.transfer(_beneficiary, _tokenAmount);
    }

    /**
     * @dev Executed when a purchase has been validated and is ready to be executed. Not necessarily emits/sends tokens.
     * @param _beneficiary Address receiving the tokens
     * @param _tokenAmount Number of tokens to be purchased
     */
    function _processPurchase(address _beneficiary, uint256 _tokenAmount)
        internal
    {
        _deliverTokens(_beneficiary, _tokenAmount);
    }

    /**
     * @dev Override for extensions that require an internal state to check for validity
     * (current user contributions, etc.)
     * @param _beneficiary Address receiving the tokens
     * @param _weiAmount Value in wei involved in the purchase
     */
    function _updatePurchasingState(address _beneficiary, uint256 _weiAmount)
        internal
    {
        // optional override
    }

    /**
     * @dev Override to extend the way in which payment token is converted to crowdsale tokens.
     * @param _weiAmount Value in wei to be converted into crowdsale tokens
     * @return Number of crowdsale tokens that can be purchased with the specified _weiAmount
     */
    function _getTokenAmount(uint256 _weiAmount)
        internal
        view
        returns (uint256)
    {
        _weiAmount = _weiAmount.sub(_getPlatformFeesAmount(_weiAmount));
        return _weiAmount.mul(rate).div(10**6);
    }

    function getTokenAmount(uint256 _weiAmount)
        external
        view
        returns (uint256)
    {
        return _getTokenAmount(_weiAmount);
    }

    /**
     * @dev plaftorm fees for each sale.
     * @param _weiAmount Value to pay in wei 
     * @return plaftorm fees
     */
    function _getPlatformFeesAmount(uint256 _weiAmount)
        internal
        view
        returns (uint256)
    {
        return _weiAmount.mul(platformFeesRate).div(10**8);
    }

    function getPlatformFeesAmount(uint256 _weiAmount)
        external
        view
        returns (uint256)
    {
        return _getPlatformFeesAmount(_weiAmount);
    }

    /**
     * @dev Determines how tokens is stored/forwarded on purchases.
     */
    function _forwardFunds(uint256 amount) internal {
        uint256 platformFees = _getPlatformFeesAmount(amount);
        paymentToken.transfer(platformFeesWallet, platformFees);
        paymentToken.transfer(wallet, amount.sub(platformFees));
    }

    /**
     * @dev Returns the address of the current owner.
     */
    function owner() public view returns (address) {
        return _owner;
    }

    /**
     * @dev Throws if called by any account other than the owner.
     */
    modifier onlyOwner() {
        require(_owner == _msgSender(), "Ownable: caller is not the owner");
        _;
    }

    /**
     * @dev Leaves the contract without owner. It will not be possible to call
     * `onlyOwner` functions anymore. Can only be called by the current owner.
     *
     * NOTE: Renouncing ownership will leave the contract without an owner,
     * thereby removing any functionality that is only available to the owner.
     */
    function renounceOwnership() public virtual onlyOwner {
        emit OwnershipTransferred(_owner, address(0));
        _owner = address(0);
    }

    /**
     * @dev Transfers ownership of the contract to a new account (`newOwner`).
     * Can only be called by the current owner.
     */
    function transferOwnership(address newOwner) public virtual onlyOwner {
        require(
            newOwner != address(0),
            "Ownable: new owner is the zero address"
        );
        emit OwnershipTransferred(_owner, newOwner);
        _owner = newOwner;
    }

    /**
     * @dev How many token units a buyer gets per wei.
     *
     * @param _rate is represented with real percentage x (10^6)
     * e.g  rate 1.5%, percentage should equal 1.5 x (10^6)
     */
    function updateRate(uint256 _rate) public onlyOwner {
        rate = _rate;
        emit RateUpdated(rate);
    }

    /**
     * @dev  How many token units platform gets per wei.
     *
     * @param _platformFeesRate is represented with real percentage x (10^6)
     * e.g  rate 1.5%, percentage should equal 1.5 x (10^6)
     */
    function updatePlatformFeesRate(uint256 _platformFeesRate) public onlyOwner {
        platformFeesRate = _platformFeesRate;
        emit PlatformFeesRateUpdated(platformFeesRate);
    }
}

// File: contracts\crowdsale\upgredableERC20Crowdsale\Initializable.sol

//SPDX-License-Identifier: MIT
pragma solidity >=0.6.0;


/**
 * @dev This is a base contract to aid in writing upgradeable contracts, or any kind of contract that will be deployed
 * behind a proxy. Since a proxied contract can't have a constructor, it's common to move constructor logic to an
 * external initializer function, usually called `initialize`. It then becomes necessary to protect this initializer
 * function so it can only be called once. The {initializer} modifier provided by this contract will have this effect.
 * 
 * TIP: To avoid leaving the proxy in an uninitialized state, the initializer function should be called as early as
 * possible by providing the encoded function call as the `_data` argument to {UpgradeableProxy-constructor}.
 * 
 * CAUTION: When used with inheritance, manual care must be taken to not invoke a parent initializer twice, or to ensure
 * that all initializers are idempotent. This is not verified automatically as constructors are by Solidity.
 */
contract Initializable {

    /**
     * @dev Indicates that the contract has been initialized.
     */
    bool private _initialized;

    /**
     * @dev Indicates that the contract is in the process of being initialized.
     */
    bool private _initializing;

    /**
     * @dev Modifier to protect an initializer function from being invoked twice.
     */
    modifier initializer() {
        require(_initializing || _isConstructor() || !_initialized, "Initializable: contract is already initialized");

        bool isTopLevelCall = !_initializing;
        if (isTopLevelCall) {
            _initializing = true;
            _initialized = true;
        }

        _;

        if (isTopLevelCall) {
            _initializing = false;
        }
    }

    /// @dev Returns true if and only if the function is running in the constructor
    function _isConstructor() private view returns (bool) {
        // extcodesize checks the size of the code stored in an address, and
        // address returns the current address. Since the code is still not
        // deployed when running a constructor, any checks on its code size will
        // yield zero, making it an effective way to detect if a contract is
        // under construction or not.
        address self = address(this);
        uint256 cs;
        // solhint-disable-next-line no-inline-assembly
        assembly { cs := extcodesize(self) }
        return cs == 0;
    }
}

// File: contracts\crowdsale\upgredableERC20Crowdsale\CrowdSaleImplementation.sol

//SPDX-License-Identifier: MIT
pragma solidity >=0.6.0;




contract CrowdSaleImplementation is
    ERC20PaymentCrowdsale,
    Initializable
{

    constructor(
        uint256 _openingTime,
        uint256 _closingTime,
        uint256 _rate,
        uint256 _platformFeesRate,
        address payable _wallet,
        address payable _platformFeesWallet,
        IERC20 _token,
        ITransferVerificationModule _whitelist,
        IERC20 _paymentToken
    )
        public
        ERC20PaymentCrowdsale(
            _openingTime,
            _closingTime,
            _rate,
            _platformFeesRate,
            _wallet,
            _platformFeesWallet,
            _token,
            _whitelist,
            _paymentToken
        )
    {}

    function initialize(
        uint256 _openingTime,
        uint256 _closingTime,
        uint256 _rate,
        uint256 _platformFeesRate,
        address payable _wallet,
        address payable _platformFeesWallet,
        IERC20 _token,
        ITransferVerificationModule _whitelist,
        IERC20 _paymentToken,
        address owner
    ) external initializer {
        require(_rate > 0, "Crowdsale: Invalid rate...");
        require(
            _wallet != address(0),
            "Crowdsale: Invalid wallet, zero address..."
        );
        require(
            _platformFeesWallet != address(0),
            "Crowdsale: Invalid platfrom wallet fees, zero address..."
        );
        require(
            address(_token) != address(0),
            "Crowdsale: Invalid ERC20 Token, zero address..."
        );
        // solium-disable-next-line security/no-block-members
        require(
            _openingTime >= block.timestamp,
            "TimedCrowdsale: Invalid openingTime..."
        );
        require(
            _closingTime >= _openingTime,
            "TimedCrowdsale: Invalid closingTime..."
        );

        openingTime = _openingTime;
        closingTime = _closingTime;
        rate = _rate;
        platformFeesRate = _platformFeesRate;
        wallet = _wallet;
        platformFeesWallet = _platformFeesWallet;
        token = _token;
        _owner = owner;
        _setWhitelistContract(_whitelist);
        _setPaymentToken(_paymentToken);

        emit ContractDeployed(
            _openingTime,
            _closingTime,
            _rate,
            _platformFeesRate,
            _wallet,
            _platformFeesWallet,
            _token,
            _whitelist,
            _paymentToken
        );
    }
}

// File: contracts\crowdsale\upgredableERC20Crowdsale\CrowdSaleImplementationV1.sol

//SPDX-License-Identifier: MIT
pragma solidity >=0.6.0;



contract CrowdSaleImplementationV1 is CrowdSaleImplementation {
    //how many tokens will be sent to the fast bonus wallet
    uint256 public fastBonusTransferRate;

    //the wallet for receiving fast bonus amounts
    address public fastBonusWallet;

    event FastBonusTransferRateUpdated(uint256 rate);
    event FastBonusWalletUpdated(address fastBonusWallet);

    /**
     *@param _openingTime crowdsate opening Epoch timestamp
     *@param _closingTime crowdsate closing Epoch timestamp
     *@param _rate is represented with real percentage x (10^6)
     *@param _platformFeesRate is represented with real percentage x (10^6)
     *@param _wallet forward collected tokens to this address
     *@param _platformFeesWallet forward collected platform fees tokens to this address
     *@param _token tokens for sale
     *@param _whitelist whitelist contract address
     *@param _paymentToken erc20 token used for purchase
     */
    constructor(
        uint256 _openingTime,
        uint256 _closingTime,
        uint256 _rate,
        uint256 _platformFeesRate,
        address payable _wallet,
        address payable _platformFeesWallet,
        IERC20 _token,
        ITransferVerificationModule _whitelist,
        IERC20 _paymentToken
    ) public CrowdSaleImplementation(_openingTime,_closingTime,_rate,_platformFeesRate,_wallet,_platformFeesWallet,_token,_whitelist,_paymentToken){
        
    }


    /**
     * @dev Extend parent behavior requiring beneficiary to be in whitelist.
     * @param _beneficiary Token beneficiary
     * @param _weiAmount Amount of wei contributed
     */
    function _preValidatePurchase(address _beneficiary, uint256 _weiAmount)
        override
        internal
        onlyWhileOpen
    {
        require(whitelist.isAutorized(_beneficiary),"Crowdsale: user not whitelisted");
        require(
            _beneficiary != address(0),
            "Crowdsale: Invalid beneficiary..."
        );
        require(_weiAmount != 0, "Crowdsale: Invalid weiAmount...");
        require(
            paymentToken.allowance(msg.sender, address(this)) >= _weiAmount,
            "Payment token allowance too low"
        );

        uint256 tokens = _getTokenAmount(_weiAmount);
        require(
            tokens <= token.balanceOf(address(this)),
            "Tokens to receive exceeds balance"
        );
    }


    /**
     * @dev Source of tokens. Override this method to modify the way in which the crowdsale
     * ultimately gets and sends its tokens.
     * @param _beneficiary Address performing the token purchase
     * @param _tokenAmount Number of tokens to be sent
     */
    function _deliverTokens(address _beneficiary, uint256 _tokenAmount)
        override
        internal
    {
        uint256 fastBonusAmount = _getFastBonusAmountToSubtract(_tokenAmount);
        token.transfer(fastBonusWallet,fastBonusAmount);
        token.transfer(_beneficiary, _tokenAmount.sub(fastBonusAmount));
    }


    

    /**
     * @dev  how many tokens will be sent to the fast bonus wallet per wei.
     *
     * @param _fastBonusTransferRate is represented with real percentage x (10^6)
     * e.g  rate 1.5%, percentage should equal 1.5 x (10^6)
     */
    function updateFastBonusTransferRate(uint256 _fastBonusTransferRate) public onlyOwner {
        fastBonusTransferRate = _fastBonusTransferRate;
        emit FastBonusTransferRateUpdated(fastBonusTransferRate);
    }

    /**
     * @dev  the wallet for receiving fast bonus amounts.
     *
     * @param _fastBonusWallet the wallet for receiving fast bonus amounts
     */
    function updateFastBonusWallet(address _fastBonusWallet) public onlyOwner {
        fastBonusWallet = _fastBonusWallet;
        emit FastBonusWalletUpdated(fastBonusWallet);
    }

    /**
     * @dev fast bonus amount to subtract for each sale.
     * @param _weiAmount Value to pay in wei 
     * @return fast bonus amount to subtract
     */
    function _getFastBonusAmountToSubtract(uint256 _weiAmount)
        internal
        view
        returns (uint256)
    {
        return _weiAmount.mul(fastBonusTransferRate).div(10**8);
    }

    function getFastBonusAmountToSubtract(uint256 _weiAmount)
        external
        view
        returns (uint256)
    {
        return _getFastBonusAmountToSubtract(_weiAmount);
    }

    /**
     * @dev Override to extend the way in which payment token is converted to crowdsale tokens.
     * @param _weiAmount Value in wei to be converted into crowdsale tokens
     * @return Number of crowdsale tokens that can be purchased with the specified _weiAmount
     */
    function getTokenToReceive(uint256 _weiAmount)
        public
        view
        returns (uint256,uint256)
    {
        uint256 amountToSend = _getTokenAmount(_weiAmount);
        uint256 fastBonusAmount = _getFastBonusAmountToSubtract(amountToSend);
        return (amountToSend.sub(fastBonusAmount),fastBonusAmount);
    }
}
