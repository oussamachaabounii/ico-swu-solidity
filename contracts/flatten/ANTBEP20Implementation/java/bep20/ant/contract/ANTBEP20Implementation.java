package bep20.ant.contract;

import io.reactivex.Flowable;
import io.reactivex.functions.Function;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.web3j.abi.EventEncoder;
import org.web3j.abi.TypeReference;
import org.web3j.abi.datatypes.Address;
import org.web3j.abi.datatypes.Bool;
import org.web3j.abi.datatypes.Event;
import org.web3j.abi.datatypes.Type;
import org.web3j.abi.datatypes.Utf8String;
import org.web3j.abi.datatypes.generated.Uint256;
import org.web3j.abi.datatypes.generated.Uint8;
import org.web3j.crypto.Credentials;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameter;
import org.web3j.protocol.core.RemoteCall;
import org.web3j.protocol.core.RemoteFunctionCall;
import org.web3j.protocol.core.methods.request.EthFilter;
import org.web3j.protocol.core.methods.response.BaseEventResponse;
import org.web3j.protocol.core.methods.response.Log;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.tx.Contract;
import org.web3j.tx.TransactionManager;
import org.web3j.tx.gas.ContractGasProvider;

/**
 * <p>Auto generated code.
 * <p><strong>Do not modify!</strong>
 * <p>Please use the <a href="https://docs.web3j.io/command_line.html">web3j command line tools</a>,
 * or the org.web3j.codegen.SolidityFunctionWrapperGenerator in the 
 * <a href="https://github.com/web3j/web3j/tree/master/codegen">codegen module</a> to update.
 *
 * <p>Generated with web3j version 1.4.1.
 */
@SuppressWarnings("rawtypes")
public class ANTBEP20Implementation extends Contract {
    public static final String BINARY = "608060405234801561001057600080fd5b5061171e806100206000396000f3fe608060405234801561001057600080fd5b50600436106101425760003560e01c80638ae482ae116100b8578063a9059cbb1161007c578063a9059cbb146104c4578063b8b94e1b146104f0578063c8fd6a6414610516578063dd62ed3e1461051e578063e31743d71461054c578063f2fde38b1461056b57610142565b80638ae482ae1461031757806395d89b411461046b57806397c34aeb14610473578063a0712d681461047b578063a457c2d71461049857610142565b8063395093511161010a578063395093511461027257806342966c681461029e5780634bf365df146102bb57806370a08231146102c3578063715018a6146102e9578063893d20e8146102f357610142565b806306fdde0314610147578063095ea7b3146101c457806318160ddd1461020457806323b872dd1461021e578063313ce56714610254575b600080fd5b61014f610591565b6040805160208082528351818301528351919283929083019185019080838360005b83811015610189578181015183820152602001610171565b50505050905090810190601f1680156101b65780820380516001836020036101000a031916815260200191505b509250505060405180910390f35b6101f0600480360360408110156101da57600080fd5b506001600160a01b038135169060200135610627565b604080519115158252519081900360200190f35b61020c610644565b60408051918252519081900360200190f35b6101f06004803603606081101561023457600080fd5b506001600160a01b0381358116916020810135909116906040013561064a565b61025c6106d1565b6040805160ff9092168252519081900360200190f35b6101f06004803603604081101561028857600080fd5b506001600160a01b0381351690602001356106da565b6101f0600480360360208110156102b457600080fd5b5035610728565b6101f0610743565b61020c600480360360208110156102d957600080fd5b50356001600160a01b0316610753565b6102f161076e565b005b6102fb61081b565b604080516001600160a01b039092168252519081900360200190f35b6102f1600480360360e081101561032d57600080fd5b81019060208101813564010000000081111561034857600080fd5b82018360208201111561035a57600080fd5b8035906020019184600183028401116401000000008311171561037c57600080fd5b91908080601f01602080910402602001604051908101604052809392919081815260200183838082843760009201919091525092959493602081019350359150506401000000008111156103cf57600080fd5b8201836020820111156103e157600080fd5b8035906020019184600183028401116401000000008311171561040357600080fd5b91908080601f0160208091040260200160405190810160405280939291908181526020018383808284376000920191909152509295505060ff8335169350505060208101359060408101351515906001600160a01b036060820135169060800135151561082f565b61014f6109b1565b6102fb610a12565b6101f06004803603602081101561049157600080fd5b5035610a21565b6101f0600480360360408110156104ae57600080fd5b506001600160a01b038135169060200135610aef565b6101f0600480360360408110156104da57600080fd5b506001600160a01b038135169060200135610b57565b6102f16004803603602081101561050657600080fd5b50356001600160a01b0316610b6b565b6101f0610c69565b61020c6004803603604081101561053457600080fd5b506001600160a01b0381358116916020013516610c79565b6102f16004803603602081101561056257600080fd5b50351515610ca4565b6102f16004803603602081101561058157600080fd5b50356001600160a01b0316610d67565b60048054604080516020601f600260001961010060018816150201909516949094049384018190048102820181019092528281526060939092909183018282801561061d5780601f106105f25761010080835404028352916020019161061d565b820191906000526020600020905b81548152906001019060200180831161060057829003601f168201915b5050505050905090565b600061063b610634610e70565b8484610e74565b50600192915050565b60035490565b6000610657848484610f60565b6106c784610663610e70565b6106c285604051806060016040528060288152602001611574602891396001600160a01b038a166000908152600260205260408120906106a1610e70565b6001600160a01b03168152602081019190915260400160002054919061115b565b610e74565b5060019392505050565b60065460ff1690565b600061063b6106e7610e70565b846106c285600260006106f8610e70565b6001600160a01b03908116825260208083019390935260409182016000908120918c1681529252902054906111f2565b600061073b610735610e70565b83611253565b506001919050565b600754600160a81b900460ff1690565b6001600160a01b031660009081526001602052604090205490565b610776610e70565b60065461010090046001600160a01b039081169116146107cb576040805162461bcd60e51b815260206004820181905260248201526000805160206115f6833981519152604482015290519081900360640190fd5b60065460405160009161010090046001600160a01b0316907f8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0908390a360068054610100600160a81b0319169055565b60065461010090046001600160a01b031690565b600054610100900460ff16806108485750610848611343565b80610856575060005460ff16155b6108915760405162461bcd60e51b815260040180806020018281038252602e8152602001806115c8602e913960400191505060405180910390fd5b600054610100900460ff161580156108bc576000805460ff1961ff0019909116610100171660011790555b60068054610100600160a81b0319166101006001600160a01b0386160217905587516108ef9060049060208b0190611471565b5086516109039060059060208a0190611471565b506006805460ff191660ff88161790556007805460ff60a81b1916600160a81b861515021790556109348386611349565b6007805460ff60a01b1916600160a01b841515810291909117918290556040805160ff92909304919091161515808352602083015280517f209800119a6f9eab592fdda84bb71d00afc97fa4b5947b5bd33435ada13de1f99281900390910190a180156109a7576000805461ff00191690555b5050505050505050565b60058054604080516020601f600260001961010060018816150201909516949094049384018190048102820181019092528281526060939092909183018282801561061d5780601f106105f25761010080835404028352916020019161061d565b6007546001600160a01b031681565b6000610a2b610e70565b60065461010090046001600160a01b03908116911614610a80576040805162461bcd60e51b815260206004820181905260248201526000805160206115f6833981519152604482015290519081900360640190fd5b600754600160a81b900460ff16610ade576040805162461bcd60e51b815260206004820152601a60248201527f7468697320746f6b656e206973206e6f74206d696e7461626c65000000000000604482015290519081900360640190fd5b61073b610ae9610e70565b83611349565b600061063b610afc610e70565b846106c28560405180606001604052806025815260200161165f6025913960026000610b26610e70565b6001600160a01b03908116825260208083019390935260409182016000908120918d1681529252902054919061115b565b600061063b610b64610e70565b8484610f60565b610b73610e70565b60065461010090046001600160a01b03908116911614610bc8576040805162461bcd60e51b815260206004820181905260248201526000805160206115f6833981519152604482015290519081900360640190fd5b6001600160a01b038116610c0d5760405162461bcd60e51b815260040180806020018281038252602c81526020018061159c602c913960400191505060405180910390fd5b6007546040516001600160a01b038084169216907fc81c06a3bde966d1c9e6126f0135faab799a67f13a291838845b6dfa67fe14e990600090a3600780546001600160a01b0319166001600160a01b0392909216919091179055565b600754600160a01b900460ff1681565b6001600160a01b03918216600090815260026020908152604080832093909416825291909152205490565b610cac610e70565b60065461010090046001600160a01b03908116911614610d01576040805162461bcd60e51b815260206004820181905260248201526000805160206115f6833981519152604482015290519081900360640190fd5b60075460408051600160a01b90920460ff161515808352602083015280517f209800119a6f9eab592fdda84bb71d00afc97fa4b5947b5bd33435ada13de1f99281900390910190a160078054911515600160a01b0260ff60a01b19909216919091179055565b610d6f610e70565b60065461010090046001600160a01b03908116911614610dc4576040805162461bcd60e51b815260206004820181905260248201526000805160206115f6833981519152604482015290519081900360640190fd5b6001600160a01b038116610e095760405162461bcd60e51b815260040180806020018281038252602681526020018061154e6026913960400191505060405180910390fd5b6006546040516001600160a01b0380841692610100900416907f8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e090600090a3600680546001600160a01b0390921661010002610100600160a81b0319909216919091179055565b3390565b6001600160a01b038316610eb95760405162461bcd60e51b815260040180806020018281038252602481526020018061152a6024913960400191505060405180910390fd5b6001600160a01b038216610efe5760405162461bcd60e51b81526004018080602001828103825260228152602001806116c76022913960400191505060405180910390fd5b6001600160a01b03808416600081815260026020908152604080832094871680845294825291829020859055815185815291517f8c5be1e5ebec7d5bd14f71427d1e84f3dd0314c0f7b2291e5b200ac8c7c3b9259281900390910190a3505050565b6001600160a01b038316610fa55760405162461bcd60e51b81526004018080602001828103825260258152602001806115056025913960400191505060405180910390fd5b6001600160a01b038216610fea5760405162461bcd60e51b815260040180806020018281038252602381526020018061163c6023913960400191505060405180910390fd5b610ff2610e70565b6007546001600160a01b03908116911614806110185750600754600160a01b900460ff16155b80611042575060065461010090046001600160a01b0316611037610e70565b6001600160a01b0316145b611093576040805162461bcd60e51b815260206004820152601960248201527f42455032303a205472616e7366657220697320636c6f73656400000000000000604482015290519081900360640190fd5b6110d081604051806060016040528060268152602001611616602691396001600160a01b038616600090815260016020526040902054919061115b565b6001600160a01b0380851660009081526001602052604080822093909355908416815220546110ff90826111f2565b6001600160a01b0380841660008181526001602090815260409182902094909455805185815290519193928716927fddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef92918290030190a3505050565b600081848411156111ea5760405162461bcd60e51b81526004018080602001828103825283818151815260200191508051906020019080838360005b838110156111af578181015183820152602001611197565b50505050905090810190601f1680156111dc5780820380516001836020036101000a031916815260200191505b509250505060405180910390fd5b505050900390565b60008282018381101561124c576040805162461bcd60e51b815260206004820152601b60248201527f536166654d6174683a206164646974696f6e206f766572666c6f770000000000604482015290519081900360640190fd5b9392505050565b6001600160a01b0382166112985760405162461bcd60e51b81526004018080602001828103825260218152602001806116846021913960400191505060405180910390fd5b6112d5816040518060600160405280602281526020016116a5602291396001600160a01b038516600090815260016020526040902054919061115b565b6001600160a01b0383166000908152600160205260409020556003546112fb908261142f565b6003556040805182815290516000916001600160a01b038516917fddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef9181900360200190a35050565b303b1590565b6001600160a01b0382166113a4576040805162461bcd60e51b815260206004820152601f60248201527f42455032303a206d696e7420746f20746865207a65726f206164647265737300604482015290519081900360640190fd5b6003546113b190826111f2565b6003556001600160a01b0382166000908152600160205260409020546113d790826111f2565b6001600160a01b03831660008181526001602090815260408083209490945583518581529351929391927fddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef9281900390910190a35050565b600061124c83836040518060400160405280601e81526020017f536166654d6174683a207375627472616374696f6e206f766572666c6f77000081525061115b565b828054600181600116156101000203166002900490600052602060002090601f016020900481019282601f106114b257805160ff19168380011785556114df565b828001600101855582156114df579182015b828111156114df5782518255916020019190600101906114c4565b506114eb9291506114ef565b5090565b5b808211156114eb57600081556001016114f056fe42455032303a207472616e736665722066726f6d20746865207a65726f206164647265737342455032303a20617070726f76652066726f6d20746865207a65726f20616464726573734f776e61626c653a206e6577206f776e657220697320746865207a65726f206164647265737342455032303a207472616e7366657220616d6f756e74206578636565647320616c6c6f77616e636543726f776453616c65436f6e74726163744164647265737320697320746865207a65726f2061646472657373496e697469616c697a61626c653a20636f6e747261637420697320616c726561647920696e697469616c697a65644f776e61626c653a2063616c6c6572206973206e6f7420746865206f776e657242455032303a207472616e7366657220616d6f756e7420657863656564732062616c616e636542455032303a207472616e7366657220746f20746865207a65726f206164647265737342455032303a2064656372656173656420616c6c6f77616e63652062656c6f77207a65726f42455032303a206275726e2066726f6d20746865207a65726f206164647265737342455032303a206275726e20616d6f756e7420657863656564732062616c616e636542455032303a20617070726f766520746f20746865207a65726f2061646472657373a264697066735822122049bacebdbcd2f9bed4bf2fb581d0e8a8b1d7d3386ce2c33bd505406221e71d7164736f6c634300060c0033";

    public static final String FUNC__CROWDSALECONTRACTADDRESS = "_crowdSaleContractAddress";

    public static final String FUNC__TRANSFERBLOCKED = "_transferBlocked";

    public static final String FUNC_ALLOWANCE = "allowance";

    public static final String FUNC_APPROVE = "approve";

    public static final String FUNC_BALANCEOF = "balanceOf";

    public static final String FUNC_BURN = "burn";

    public static final String FUNC_CHANGECROWDSALECONTRACTADDRESS = "changeCrowdSaleContractAddress";

    public static final String FUNC_DECIMALS = "decimals";

    public static final String FUNC_DECREASEALLOWANCE = "decreaseAllowance";

    public static final String FUNC_GETOWNER = "getOwner";

    public static final String FUNC_INCREASEALLOWANCE = "increaseAllowance";

    public static final String FUNC_INITIALIZE = "initialize";

    public static final String FUNC_MINT = "mint";

    public static final String FUNC_MINTABLE = "mintable";

    public static final String FUNC_NAME = "name";

    public static final String FUNC_RENOUNCEOWNERSHIP = "renounceOwnership";

    public static final String FUNC_SETTRANSFERBLOCKED = "setTransferBlocked";

    public static final String FUNC_SYMBOL = "symbol";

    public static final String FUNC_TOTALSUPPLY = "totalSupply";

    public static final String FUNC_TRANSFER = "transfer";

    public static final String FUNC_TRANSFERFROM = "transferFrom";

    public static final String FUNC_TRANSFEROWNERSHIP = "transferOwnership";

    public static final Event APPROVAL_EVENT = new Event("Approval", 
            Arrays.<TypeReference<?>>asList(new TypeReference<Address>(true) {}, new TypeReference<Address>(true) {}, new TypeReference<Uint256>() {}));
    ;

    public static final Event CROWDSALECONTRACTADDRESSCHANGED_EVENT = new Event("CrowdSaleContractAddressChanged", 
            Arrays.<TypeReference<?>>asList(new TypeReference<Address>(true) {}, new TypeReference<Address>(true) {}));
    ;

    public static final Event OWNERSHIPTRANSFERRED_EVENT = new Event("OwnershipTransferred", 
            Arrays.<TypeReference<?>>asList(new TypeReference<Address>(true) {}, new TypeReference<Address>(true) {}));
    ;

    public static final Event TRANSFER_EVENT = new Event("Transfer", 
            Arrays.<TypeReference<?>>asList(new TypeReference<Address>(true) {}, new TypeReference<Address>(true) {}, new TypeReference<Uint256>() {}));
    ;

    public static final Event TRANSFERBLOCKEDCHANGED_EVENT = new Event("transferBlockedChanged", 
            Arrays.<TypeReference<?>>asList(new TypeReference<Bool>() {}, new TypeReference<Bool>() {}));
    ;

    @Deprecated
    protected ANTBEP20Implementation(String contractAddress, Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        super(BINARY, contractAddress, web3j, credentials, gasPrice, gasLimit);
    }

    protected ANTBEP20Implementation(String contractAddress, Web3j web3j, Credentials credentials, ContractGasProvider contractGasProvider) {
        super(BINARY, contractAddress, web3j, credentials, contractGasProvider);
    }

    @Deprecated
    protected ANTBEP20Implementation(String contractAddress, Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        super(BINARY, contractAddress, web3j, transactionManager, gasPrice, gasLimit);
    }

    protected ANTBEP20Implementation(String contractAddress, Web3j web3j, TransactionManager transactionManager, ContractGasProvider contractGasProvider) {
        super(BINARY, contractAddress, web3j, transactionManager, contractGasProvider);
    }

    public List<ApprovalEventResponse> getApprovalEvents(TransactionReceipt transactionReceipt) {
        List<Contract.EventValuesWithLog> valueList = extractEventParametersWithLog(APPROVAL_EVENT, transactionReceipt);
        ArrayList<ApprovalEventResponse> responses = new ArrayList<ApprovalEventResponse>(valueList.size());
        for (Contract.EventValuesWithLog eventValues : valueList) {
            ApprovalEventResponse typedResponse = new ApprovalEventResponse();
            typedResponse.log = eventValues.getLog();
            typedResponse.owner = (String) eventValues.getIndexedValues().get(0).getValue();
            typedResponse.spender = (String) eventValues.getIndexedValues().get(1).getValue();
            typedResponse.value = (BigInteger) eventValues.getNonIndexedValues().get(0).getValue();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Flowable<ApprovalEventResponse> approvalEventFlowable(EthFilter filter) {
        return web3j.ethLogFlowable(filter).map(new Function<Log, ApprovalEventResponse>() {
            @Override
            public ApprovalEventResponse apply(Log log) {
                Contract.EventValuesWithLog eventValues = extractEventParametersWithLog(APPROVAL_EVENT, log);
                ApprovalEventResponse typedResponse = new ApprovalEventResponse();
                typedResponse.log = log;
                typedResponse.owner = (String) eventValues.getIndexedValues().get(0).getValue();
                typedResponse.spender = (String) eventValues.getIndexedValues().get(1).getValue();
                typedResponse.value = (BigInteger) eventValues.getNonIndexedValues().get(0).getValue();
                return typedResponse;
            }
        });
    }

    public Flowable<ApprovalEventResponse> approvalEventFlowable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(APPROVAL_EVENT));
        return approvalEventFlowable(filter);
    }

    public List<CrowdSaleContractAddressChangedEventResponse> getCrowdSaleContractAddressChangedEvents(TransactionReceipt transactionReceipt) {
        List<Contract.EventValuesWithLog> valueList = extractEventParametersWithLog(CROWDSALECONTRACTADDRESSCHANGED_EVENT, transactionReceipt);
        ArrayList<CrowdSaleContractAddressChangedEventResponse> responses = new ArrayList<CrowdSaleContractAddressChangedEventResponse>(valueList.size());
        for (Contract.EventValuesWithLog eventValues : valueList) {
            CrowdSaleContractAddressChangedEventResponse typedResponse = new CrowdSaleContractAddressChangedEventResponse();
            typedResponse.log = eventValues.getLog();
            typedResponse.oldCrowdSaleContractAddress = (String) eventValues.getIndexedValues().get(0).getValue();
            typedResponse.newCrowdSaleContractAddress = (String) eventValues.getIndexedValues().get(1).getValue();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Flowable<CrowdSaleContractAddressChangedEventResponse> crowdSaleContractAddressChangedEventFlowable(EthFilter filter) {
        return web3j.ethLogFlowable(filter).map(new Function<Log, CrowdSaleContractAddressChangedEventResponse>() {
            @Override
            public CrowdSaleContractAddressChangedEventResponse apply(Log log) {
                Contract.EventValuesWithLog eventValues = extractEventParametersWithLog(CROWDSALECONTRACTADDRESSCHANGED_EVENT, log);
                CrowdSaleContractAddressChangedEventResponse typedResponse = new CrowdSaleContractAddressChangedEventResponse();
                typedResponse.log = log;
                typedResponse.oldCrowdSaleContractAddress = (String) eventValues.getIndexedValues().get(0).getValue();
                typedResponse.newCrowdSaleContractAddress = (String) eventValues.getIndexedValues().get(1).getValue();
                return typedResponse;
            }
        });
    }

    public Flowable<CrowdSaleContractAddressChangedEventResponse> crowdSaleContractAddressChangedEventFlowable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(CROWDSALECONTRACTADDRESSCHANGED_EVENT));
        return crowdSaleContractAddressChangedEventFlowable(filter);
    }

    public List<OwnershipTransferredEventResponse> getOwnershipTransferredEvents(TransactionReceipt transactionReceipt) {
        List<Contract.EventValuesWithLog> valueList = extractEventParametersWithLog(OWNERSHIPTRANSFERRED_EVENT, transactionReceipt);
        ArrayList<OwnershipTransferredEventResponse> responses = new ArrayList<OwnershipTransferredEventResponse>(valueList.size());
        for (Contract.EventValuesWithLog eventValues : valueList) {
            OwnershipTransferredEventResponse typedResponse = new OwnershipTransferredEventResponse();
            typedResponse.log = eventValues.getLog();
            typedResponse.previousOwner = (String) eventValues.getIndexedValues().get(0).getValue();
            typedResponse.newOwner = (String) eventValues.getIndexedValues().get(1).getValue();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Flowable<OwnershipTransferredEventResponse> ownershipTransferredEventFlowable(EthFilter filter) {
        return web3j.ethLogFlowable(filter).map(new Function<Log, OwnershipTransferredEventResponse>() {
            @Override
            public OwnershipTransferredEventResponse apply(Log log) {
                Contract.EventValuesWithLog eventValues = extractEventParametersWithLog(OWNERSHIPTRANSFERRED_EVENT, log);
                OwnershipTransferredEventResponse typedResponse = new OwnershipTransferredEventResponse();
                typedResponse.log = log;
                typedResponse.previousOwner = (String) eventValues.getIndexedValues().get(0).getValue();
                typedResponse.newOwner = (String) eventValues.getIndexedValues().get(1).getValue();
                return typedResponse;
            }
        });
    }

    public Flowable<OwnershipTransferredEventResponse> ownershipTransferredEventFlowable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(OWNERSHIPTRANSFERRED_EVENT));
        return ownershipTransferredEventFlowable(filter);
    }

    public List<TransferEventResponse> getTransferEvents(TransactionReceipt transactionReceipt) {
        List<Contract.EventValuesWithLog> valueList = extractEventParametersWithLog(TRANSFER_EVENT, transactionReceipt);
        ArrayList<TransferEventResponse> responses = new ArrayList<TransferEventResponse>(valueList.size());
        for (Contract.EventValuesWithLog eventValues : valueList) {
            TransferEventResponse typedResponse = new TransferEventResponse();
            typedResponse.log = eventValues.getLog();
            typedResponse.from = (String) eventValues.getIndexedValues().get(0).getValue();
            typedResponse.to = (String) eventValues.getIndexedValues().get(1).getValue();
            typedResponse.value = (BigInteger) eventValues.getNonIndexedValues().get(0).getValue();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Flowable<TransferEventResponse> transferEventFlowable(EthFilter filter) {
        return web3j.ethLogFlowable(filter).map(new Function<Log, TransferEventResponse>() {
            @Override
            public TransferEventResponse apply(Log log) {
                Contract.EventValuesWithLog eventValues = extractEventParametersWithLog(TRANSFER_EVENT, log);
                TransferEventResponse typedResponse = new TransferEventResponse();
                typedResponse.log = log;
                typedResponse.from = (String) eventValues.getIndexedValues().get(0).getValue();
                typedResponse.to = (String) eventValues.getIndexedValues().get(1).getValue();
                typedResponse.value = (BigInteger) eventValues.getNonIndexedValues().get(0).getValue();
                return typedResponse;
            }
        });
    }

    public Flowable<TransferEventResponse> transferEventFlowable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(TRANSFER_EVENT));
        return transferEventFlowable(filter);
    }

    public List<TransferBlockedChangedEventResponse> getTransferBlockedChangedEvents(TransactionReceipt transactionReceipt) {
        List<Contract.EventValuesWithLog> valueList = extractEventParametersWithLog(TRANSFERBLOCKEDCHANGED_EVENT, transactionReceipt);
        ArrayList<TransferBlockedChangedEventResponse> responses = new ArrayList<TransferBlockedChangedEventResponse>(valueList.size());
        for (Contract.EventValuesWithLog eventValues : valueList) {
            TransferBlockedChangedEventResponse typedResponse = new TransferBlockedChangedEventResponse();
            typedResponse.log = eventValues.getLog();
            typedResponse.oldValue = (Boolean) eventValues.getNonIndexedValues().get(0).getValue();
            typedResponse.newValue = (Boolean) eventValues.getNonIndexedValues().get(1).getValue();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Flowable<TransferBlockedChangedEventResponse> transferBlockedChangedEventFlowable(EthFilter filter) {
        return web3j.ethLogFlowable(filter).map(new Function<Log, TransferBlockedChangedEventResponse>() {
            @Override
            public TransferBlockedChangedEventResponse apply(Log log) {
                Contract.EventValuesWithLog eventValues = extractEventParametersWithLog(TRANSFERBLOCKEDCHANGED_EVENT, log);
                TransferBlockedChangedEventResponse typedResponse = new TransferBlockedChangedEventResponse();
                typedResponse.log = log;
                typedResponse.oldValue = (Boolean) eventValues.getNonIndexedValues().get(0).getValue();
                typedResponse.newValue = (Boolean) eventValues.getNonIndexedValues().get(1).getValue();
                return typedResponse;
            }
        });
    }

    public Flowable<TransferBlockedChangedEventResponse> transferBlockedChangedEventFlowable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(TRANSFERBLOCKEDCHANGED_EVENT));
        return transferBlockedChangedEventFlowable(filter);
    }

    public RemoteFunctionCall<String> _crowdSaleContractAddress() {
        final org.web3j.abi.datatypes.Function function = new org.web3j.abi.datatypes.Function(FUNC__CROWDSALECONTRACTADDRESS, 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Address>() {}));
        return executeRemoteCallSingleValueReturn(function, String.class);
    }

    public RemoteFunctionCall<Boolean> _transferBlocked() {
        final org.web3j.abi.datatypes.Function function = new org.web3j.abi.datatypes.Function(FUNC__TRANSFERBLOCKED, 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Bool>() {}));
        return executeRemoteCallSingleValueReturn(function, Boolean.class);
    }

    public RemoteFunctionCall<BigInteger> allowance(String owner, String spender) {
        final org.web3j.abi.datatypes.Function function = new org.web3j.abi.datatypes.Function(FUNC_ALLOWANCE, 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.Address(160, owner), 
                new org.web3j.abi.datatypes.Address(160, spender)), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}));
        return executeRemoteCallSingleValueReturn(function, BigInteger.class);
    }

    public RemoteFunctionCall<TransactionReceipt> approve(String spender, BigInteger amount) {
        final org.web3j.abi.datatypes.Function function = new org.web3j.abi.datatypes.Function(
                FUNC_APPROVE, 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.Address(160, spender), 
                new org.web3j.abi.datatypes.generated.Uint256(amount)), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteFunctionCall<BigInteger> balanceOf(String account) {
        final org.web3j.abi.datatypes.Function function = new org.web3j.abi.datatypes.Function(FUNC_BALANCEOF, 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.Address(160, account)), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}));
        return executeRemoteCallSingleValueReturn(function, BigInteger.class);
    }

    public RemoteFunctionCall<TransactionReceipt> burn(BigInteger amount) {
        final org.web3j.abi.datatypes.Function function = new org.web3j.abi.datatypes.Function(
                FUNC_BURN, 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.generated.Uint256(amount)), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteFunctionCall<TransactionReceipt> changeCrowdSaleContractAddress(String newCrowdSaleContractAddress) {
        final org.web3j.abi.datatypes.Function function = new org.web3j.abi.datatypes.Function(
                FUNC_CHANGECROWDSALECONTRACTADDRESS, 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.Address(160, newCrowdSaleContractAddress)), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteFunctionCall<BigInteger> decimals() {
        final org.web3j.abi.datatypes.Function function = new org.web3j.abi.datatypes.Function(FUNC_DECIMALS, 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint8>() {}));
        return executeRemoteCallSingleValueReturn(function, BigInteger.class);
    }

    public RemoteFunctionCall<TransactionReceipt> decreaseAllowance(String spender, BigInteger subtractedValue) {
        final org.web3j.abi.datatypes.Function function = new org.web3j.abi.datatypes.Function(
                FUNC_DECREASEALLOWANCE, 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.Address(160, spender), 
                new org.web3j.abi.datatypes.generated.Uint256(subtractedValue)), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteFunctionCall<String> getOwner() {
        final org.web3j.abi.datatypes.Function function = new org.web3j.abi.datatypes.Function(FUNC_GETOWNER, 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Address>() {}));
        return executeRemoteCallSingleValueReturn(function, String.class);
    }

    public RemoteFunctionCall<TransactionReceipt> increaseAllowance(String spender, BigInteger addedValue) {
        final org.web3j.abi.datatypes.Function function = new org.web3j.abi.datatypes.Function(
                FUNC_INCREASEALLOWANCE, 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.Address(160, spender), 
                new org.web3j.abi.datatypes.generated.Uint256(addedValue)), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteFunctionCall<TransactionReceipt> initialize(String name, String symbol, BigInteger decimals, BigInteger amount, Boolean mintable, String owner, Boolean transferBlocked) {
        final org.web3j.abi.datatypes.Function function = new org.web3j.abi.datatypes.Function(
                FUNC_INITIALIZE, 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.Utf8String(name), 
                new org.web3j.abi.datatypes.Utf8String(symbol), 
                new org.web3j.abi.datatypes.generated.Uint8(decimals), 
                new org.web3j.abi.datatypes.generated.Uint256(amount), 
                new org.web3j.abi.datatypes.Bool(mintable), 
                new org.web3j.abi.datatypes.Address(160, owner), 
                new org.web3j.abi.datatypes.Bool(transferBlocked)), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteFunctionCall<TransactionReceipt> mint(BigInteger amount) {
        final org.web3j.abi.datatypes.Function function = new org.web3j.abi.datatypes.Function(
                FUNC_MINT, 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.generated.Uint256(amount)), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteFunctionCall<Boolean> mintable() {
        final org.web3j.abi.datatypes.Function function = new org.web3j.abi.datatypes.Function(FUNC_MINTABLE, 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Bool>() {}));
        return executeRemoteCallSingleValueReturn(function, Boolean.class);
    }

    public RemoteFunctionCall<String> name() {
        final org.web3j.abi.datatypes.Function function = new org.web3j.abi.datatypes.Function(FUNC_NAME, 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Utf8String>() {}));
        return executeRemoteCallSingleValueReturn(function, String.class);
    }

    public RemoteFunctionCall<TransactionReceipt> renounceOwnership() {
        final org.web3j.abi.datatypes.Function function = new org.web3j.abi.datatypes.Function(
                FUNC_RENOUNCEOWNERSHIP, 
                Arrays.<Type>asList(), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteFunctionCall<TransactionReceipt> setTransferBlocked(Boolean transferBlocked) {
        final org.web3j.abi.datatypes.Function function = new org.web3j.abi.datatypes.Function(
                FUNC_SETTRANSFERBLOCKED, 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.Bool(transferBlocked)), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteFunctionCall<String> symbol() {
        final org.web3j.abi.datatypes.Function function = new org.web3j.abi.datatypes.Function(FUNC_SYMBOL, 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Utf8String>() {}));
        return executeRemoteCallSingleValueReturn(function, String.class);
    }

    public RemoteFunctionCall<BigInteger> totalSupply() {
        final org.web3j.abi.datatypes.Function function = new org.web3j.abi.datatypes.Function(FUNC_TOTALSUPPLY, 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}));
        return executeRemoteCallSingleValueReturn(function, BigInteger.class);
    }

    public RemoteFunctionCall<TransactionReceipt> transfer(String recipient, BigInteger amount) {
        final org.web3j.abi.datatypes.Function function = new org.web3j.abi.datatypes.Function(
                FUNC_TRANSFER, 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.Address(160, recipient), 
                new org.web3j.abi.datatypes.generated.Uint256(amount)), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteFunctionCall<TransactionReceipt> transferFrom(String sender, String recipient, BigInteger amount) {
        final org.web3j.abi.datatypes.Function function = new org.web3j.abi.datatypes.Function(
                FUNC_TRANSFERFROM, 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.Address(160, sender), 
                new org.web3j.abi.datatypes.Address(160, recipient), 
                new org.web3j.abi.datatypes.generated.Uint256(amount)), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteFunctionCall<TransactionReceipt> transferOwnership(String newOwner) {
        final org.web3j.abi.datatypes.Function function = new org.web3j.abi.datatypes.Function(
                FUNC_TRANSFEROWNERSHIP, 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.Address(160, newOwner)), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    @Deprecated
    public static ANTBEP20Implementation load(String contractAddress, Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        return new ANTBEP20Implementation(contractAddress, web3j, credentials, gasPrice, gasLimit);
    }

    @Deprecated
    public static ANTBEP20Implementation load(String contractAddress, Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        return new ANTBEP20Implementation(contractAddress, web3j, transactionManager, gasPrice, gasLimit);
    }

    public static ANTBEP20Implementation load(String contractAddress, Web3j web3j, Credentials credentials, ContractGasProvider contractGasProvider) {
        return new ANTBEP20Implementation(contractAddress, web3j, credentials, contractGasProvider);
    }

    public static ANTBEP20Implementation load(String contractAddress, Web3j web3j, TransactionManager transactionManager, ContractGasProvider contractGasProvider) {
        return new ANTBEP20Implementation(contractAddress, web3j, transactionManager, contractGasProvider);
    }

    public static RemoteCall<ANTBEP20Implementation> deploy(Web3j web3j, Credentials credentials, ContractGasProvider contractGasProvider) {
        return deployRemoteCall(ANTBEP20Implementation.class, web3j, credentials, contractGasProvider, BINARY, "");
    }

    public static RemoteCall<ANTBEP20Implementation> deploy(Web3j web3j, TransactionManager transactionManager, ContractGasProvider contractGasProvider) {
        return deployRemoteCall(ANTBEP20Implementation.class, web3j, transactionManager, contractGasProvider, BINARY, "");
    }

    @Deprecated
    public static RemoteCall<ANTBEP20Implementation> deploy(Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        return deployRemoteCall(ANTBEP20Implementation.class, web3j, credentials, gasPrice, gasLimit, BINARY, "");
    }

    @Deprecated
    public static RemoteCall<ANTBEP20Implementation> deploy(Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        return deployRemoteCall(ANTBEP20Implementation.class, web3j, transactionManager, gasPrice, gasLimit, BINARY, "");
    }

    public static class ApprovalEventResponse extends BaseEventResponse {
        public String owner;

        public String spender;

        public BigInteger value;
    }

    public static class CrowdSaleContractAddressChangedEventResponse extends BaseEventResponse {
        public String oldCrowdSaleContractAddress;

        public String newCrowdSaleContractAddress;
    }

    public static class OwnershipTransferredEventResponse extends BaseEventResponse {
        public String previousOwner;

        public String newOwner;
    }

    public static class TransferEventResponse extends BaseEventResponse {
        public String from;

        public String to;

        public BigInteger value;
    }

    public static class TransferBlockedChangedEventResponse extends BaseEventResponse {
        public Boolean oldValue;

        public Boolean newValue;
    }
}
