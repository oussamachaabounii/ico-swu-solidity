// File: contracts\modules\transfer\ITransferVerificationModule.sol

// SPDX-License-Identifier: MIT
pragma solidity >=0.6.0;
/**
* @notice module to check if user can do transfert or not
 */
interface ITransferVerificationModule{
    function isAutorized(address _address) external view returns (bool);
}

// File: contracts\access\Roles.sol

// SPDX-License-Identifier: MIT
pragma solidity >=0.6.0;

/**
 * @title Roles
 * @dev Library for managing addresses assigned to a Role.
 */
library Roles {
    struct Role {
        mapping (address => bool) bearer;
    }

    /**
     * @dev Give an account access to this role.
     */
    function add(Role storage role, address account) internal {
        require(!has(role, account), "Roles: account already has role");
        role.bearer[account] = true;
    }

    /**
     * @dev Remove an account's access to this role.
     */
    function remove(Role storage role, address account) internal {
        require(has(role, account), "Roles: account does not have role");
        role.bearer[account] = false;
    }

    /**
     * @dev Check if an account has this role.
     * @return bool
     */
    function has(Role storage role, address account) internal view returns (bool) {
        require(account != address(0), "Roles: account is the zero address");
        return role.bearer[account];
    }
}

// File: contracts\access\roles\WhitelistAdminRole.sol

// SPDX-License-Identifier: MIT
pragma solidity >=0.6.0;


/**
 * @title WhitelistAdminRole
 * @dev WhitelistAdmins are responsible for assigning and removing Whitelisted accounts.
 */
contract WhitelistAdminRole {
    using Roles for Roles.Role;

    event WhitelistAdminAdded(address indexed account);
    event WhitelistAdminRemoved(address indexed account);

    Roles.Role private _whitelistAdmins;

    constructor () internal {
        _addWhitelistAdmin(msg.sender);
    }

    modifier onlyWhitelistAdmin() {
        require(_whitelistAdmins.has(msg.sender), "WhitelistAdminRole: caller does not have the WhitelistAdmin role");
        _;
    }

    function isWhitelistAdmin(address account) public view returns (bool) {
        return _whitelistAdmins.has(account);
    }

    function addWhitelistAdmin(address account) public onlyWhitelistAdmin {
        _addWhitelistAdmin(account);
    }

    function renounceWhitelistAdmin() public {
        _removeWhitelistAdmin(msg.sender);
    }

    function _addWhitelistAdmin(address account) internal {
        _whitelistAdmins.add(account);
        emit WhitelistAdminAdded(account);
    }

    function _removeWhitelistAdmin(address account) internal {
        _whitelistAdmins.remove(account);
        emit WhitelistAdminRemoved(account);
    }
}

// File: contracts\access\roles\WhiteListedRole.sol

// SPDX-License-Identifier: MIT
pragma solidity >=0.6.0;



/**
 * @title WhitelistedRole
 * @dev Whitelisted accounts have been approved by a WhitelistAdmin to perform certain actions (e.g. participate in a
 * crowdsale). This role is special in that the only accounts that can add it are WhitelistAdmins (who can also remove
 * it), and not Whitelisteds themselves.
 */
contract WhiteListedRole is WhitelistAdminRole {
    using Roles for Roles.Role;

    event WhitelistedAdded(address indexed account);
    event WhitelistedRemoved(address indexed account);

    Roles.Role private _whitelisteds;

    modifier onlyWhitelisted() {
        require(_whitelisteds.has(msg.sender), "WhitelistedRole: caller does not have the Whitelisted role");
        _;
    }

    function isWhitelisted(address account) external view returns (bool) {
        return _whitelisteds.has(account);
    }

    function _isWhitelisted(address account) internal view returns (bool) {
        return _whitelisteds.has(account);
    }

    function addWhitelisted(address account) external onlyWhitelistAdmin {
        _addWhitelisted(account);
    }

    function removeWhitelisted(address account) external onlyWhitelistAdmin {
        _removeWhitelisted(account);
    }

    function renounceWhitelisted() external {
        _removeWhitelisted(msg.sender);
    }

    function _addWhitelisted(address account) internal {
        _whitelisteds.add(account);
        emit WhitelistedAdded(account);
    }

    function _removeWhitelisted(address account) internal {
        _whitelisteds.remove(account);
        emit WhitelistedRemoved(account);
    }
}

// File: contracts\modules\transfer\whiteList\WhiteList.sol

// SPDX-License-Identifier: MIT
pragma solidity >=0.6.0;



contract WhiteList is ITransferVerificationModule, WhiteListedRole{
    function isAutorized(address _address) external override(ITransferVerificationModule) view returns (bool){
        return _isWhitelisted(_address);
    }
}
